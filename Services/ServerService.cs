﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.IO;
using System.Xml.Serialization;
using FirebirdSql.Data.FirebirdClient;
using Topshelf;

namespace RepServer
{
    public class ServerService
    {
        private Stream stream;
        private XmlSerializer xmlSerializer;
        private Settings settings;
        private ServiceHost host;
        private Uri baseAddress;

        public ServerService()
        {

        }

        public void Start()
        {
            stream = new FileStream("server_settings.xml", FileMode.Open);
            xmlSerializer = new XmlSerializer(typeof(Settings));
            settings = (Settings)xmlSerializer.Deserialize(stream);
            stream.Close();
            baseAddress = new Uri(settings.ServiceUrl + ":" + settings.ServicePort.ToString());
            // Create the ServiceHost.
            host = new ServiceHost(typeof(ReplicationService), baseAddress);
            // Enable metadata publishing.
            ServiceMetadataBehavior smb = new ServiceMetadataBehavior
            {
                HttpGetEnabled = true,
                MetadataExporter = { PolicyVersion = PolicyVersion.Policy15 }
            };
            //ServiceBehaviorAttribute sba = new ServiceBehaviorAttribute();
            //sba.
            host.Description.Behaviors.Add(smb);

            // Open the ServiceHost to start listening for messages. Since
            // no endpoints are explicitly configured, the runtime will create
            // one endpoint per base address for each service contract implemented
            // by the service.
            host.Open();
            //Console.WriteLine("The service is ready at {0}", baseAddress);
            //Console.WriteLine("Press <Enter> to stop the service.");
            //Console.WriteLine("Server provider: " + settings.ServiceProvider);
            //Console.WriteLine("Replication source: " + settings.ServiceConnectionString);
            //Console.ReadLine();
        }

        public void Stop()
        {
            host.Close();
        }
    }
}

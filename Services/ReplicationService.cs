﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.IO;
using System.Xml.Serialization;
using FirebirdSql.Data.FirebirdClient;
using Topshelf;

namespace RepServer
{
    [ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    public class ReplicationService : IReplicationService
    {

        public Settings ServerSettings
        {
            get;
            set;
        }

        //public MkrkReplicationSettings ServerMkrkReplicationSettings
        //{
        //    get; set;
        //}

        public ReplicationService()
        {
            Stream stream = new FileStream("server_settings.xml", FileMode.Open);
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(Settings));
            Settings settings = (Settings)xmlSerializer.Deserialize(stream);
            this.ServerSettings = settings;
            stream.Close();
        }

        public DateTime GetMaxDate(string entity, string regionId)
        {
            //var connsb = new SqlConnectionStringBuilder()
            //SqlConnection conn = new SqlConnection("server=ZLOJ-PC\\SRVZERO;database=ASRK_RF_MSK;uid=sa;pwd=123");
            DateTime dt = new DateTime(2099, 12, 12);
            string cmdText = "";

            if (entity.ToUpper() == "PROTOCOLS")
            {
                if (this.ServerSettings.ServiceProvider.ToUpper() == "MSSQL")
                {
                    cmdText = 
                        @"select coalesce(max(dt_modify), '1990-01-01') as max_dt_modify 
                          from RK_PROTOCOLS";
                }

                if (this.ServerSettings.ServiceProvider.ToUpper() == "FIREBIRD")
                {
                    cmdText = 
                        @"select coalesce(max(dt_modify), '1990-01-01') as max_dt_modify 
                          from RES_PROTOCOL_IZM_TP";
                }
            }

            if (entity.ToUpper() == "ACTS")
            {
                if (this.ServerSettings.ServiceProvider.ToUpper() == "MSSQL")
                {
                    cmdText = 
                        @"select coalesce(max(dt_modify), '1990-01-01') as max_dt_modify 
                          from RK_ACT_DATA";
                }

                if (this.ServerSettings.ServiceProvider.ToUpper() == "FIREBIRD")
                {
                    cmdText = 
                        @"select coalesce(max(dt_modify), '1990-01-01') as max_dt_modify 
                          from ACT_DATA";
                }
            }

            if (this.ServerSettings.ServiceProvider.ToUpper() == "MSSQL")
            {
                SqlConnection conn = new SqlConnection(this.ServerSettings.ServiceConnectionString);
                SqlCommand data = new SqlCommand(cmdText, conn);

                conn.Open();
                data.CommandTimeout = 1800000;
                SqlDataReader reader = data.ExecuteReader();
                var read = reader.Read();

                if (read)
                {
                    dt = reader.GetDateTime(0);
                }
            }

            if (this.ServerSettings.ServiceProvider.ToUpper() == "FIREBIRD")
            {
                FbConnection conn = new FbConnection(this.ServerSettings.ServiceConnectionString);
                FbCommand data = new FbCommand(cmdText, conn);

                conn.Open();
                data.CommandTimeout = 1800000;
                FbDataReader reader = data.ExecuteReader();
                var read = reader.Read();

                if (read)
                {
                    dt = reader.GetDateTime(0);
                }
            }

            return dt;
        }

        public IList<string> GetEntityIdList(string entity, DateTime maxdate)
        {
            IList<string> idlist = new List<string>();
            string cmdText = "";

            if (entity.ToUpper() == "PROTOCOLS")
            {
                if (this.ServerSettings.ServiceProvider.ToUpper() == "MSSQL")
                {
                    cmdText =
                        @"select P.ID_PROTOCOL as ID 
                          from RK_PROTOCOLS P 
                          where (P.ID_PROTOCOL <> 0) 
                            AND (P.DT_MODIFY > @maxdate or P.DT_MODIFY is null)";
                }

                if (this.ServerSettings.ServiceProvider.ToUpper() == "FIREBIRD")
                {
                    cmdText =
                        @"select P.ID_PROTOCOL as ID 
                          from RES_PROTOCOL_IZM_TP P 
                          where (P.ID_PROTOCOL <> 0) 
                            AND (P.DT_MODIFY > @maxdate or P.DT_MODIFY is null)";
                    //"select P.id_PROTOCOL as id from RES_PROTOCOL_IZM_TP P where P.id_protocol = 481767717106000136 ";
                }
            }

            if (entity.ToUpper() == "ACTS")
            {
                if (this.ServerSettings.ServiceProvider.ToUpper() == "MSSQL")
                {
                    cmdText = 
                        @"select A.id_act_data 
                          from rk_act_data A 
                          where (A.id_act_data <> 0) 
                            AND (A.DT_MODIFY > @maxdate or A.DT_MODIFY is null)";
                }

                if (this.ServerSettings.ServiceProvider.ToUpper() == "FIREBIRD")
                {
                    cmdText = 
                        @"select A.ID_ACT_DATA 
                          from act_data A 
                          where (A.ID_ACT_DATA <> 0) 
                            AND (A.DT_MODIFY > @maxdate or A.DT_MODIFY is null)";
                    /*
                                    ID_ACT_DATA
                                    2000136
                                    19000136
                                    22000136
                                    26000136
                                    10000136
                                    1761000136
                                    18000136
                                    27000136
                                    14000136
                                    20000136
                        */
                }
            }

            if (entity.ToUpper() == "EMPLOYEE")
            {
                if (this.ServerSettings.ServiceProvider.ToUpper() == "MSSQL")
                {
                    cmdText =
                        " ";
                }

                if (this.ServerSettings.ServiceProvider.ToUpper() == "FIREBIRD")
                {
                    cmdText =
                        @"select ID_SOTRUDNIK as ID 
                          from SOTRUDNIK S 
                          where (S.ID_SOTRUDNIK <> 0) 
                            AND (S.DT_MODIFY > @maxdate or S.DT_MODIFY is null)";
                }
            }

            if (entity.ToUpper() == "RKO")
            {
                if (this.ServerSettings.ServiceProvider.ToUpper() == "MSSQL")
                {
                    cmdText =
                        " ";
                }

                if (this.ServerSettings.ServiceProvider.ToUpper() == "FIREBIRD")
                {
                    cmdText =
                        @"select R.ID_SR_IZM as ID 
                          from RK_SR_IZM R 
                          where (R.ID_SR_IZM <> 0) 
                            AND (R.DT_MODIFY > @maxdate or R.DT_MODIFY is null)";
                }
            }

            if (entity.ToUpper() == "SUBDIVISIONS")
            {
                if (this.ServerSettings.ServiceProvider.ToUpper() == "MSSQL")
                {
                    cmdText =
                        " ";
                }

                if (this.ServerSettings.ServiceProvider.ToUpper() == "FIREBIRD")
                {
                    cmdText =
                        @"select P.id_podrazdelenie as ID 
                          from podrazdelenie P 
                          where (P.IS_RKP = 0) 
                            AND (P.id_podrazdelenie <> 0) 
                            AND (P.DT_MODIFY > @maxdate or P.DT_MODIFY is null)";
                }
            }

            if (entity.ToUpper() == "PLANS")
            {
                if (this.ServerSettings.ServiceProvider.ToUpper() == "MSSQL")
                {
                    cmdText =
                        " ";
                }

                if (this.ServerSettings.ServiceProvider.ToUpper() == "FIREBIRD")
                {
                    cmdText =
                        @"select P.ID_RK_AGGR_PLANDETAIL as ID 
                          from RK_AGGR_PLANDETAIL P 
                          where (P.ID_RK_AGGR_PLANDETAIL <> 0) 
                            AND (P.DT_MODIFY > @maxdate or P.DT_MODIFY is null)";
                }
            }

            if (entity.ToUpper() == "MKRK")
            {
                if (this.ServerSettings.ServiceProvider.ToUpper() == "MSSQL")
                {
                    cmdText =
                        " ";
                }

                if (this.ServerSettings.ServiceProvider.ToUpper() == "FIREBIRD")
                {
                    cmdText =
                        @"select O.ID_MON_OBJECTs as ID 
                          from MON_OBJECTS O 
                          where (O.id_mon_objects <> 0) 
                            AND (O.last_time > @maxdate  or O.last_time is null)";
                }
            }

            if (entity.ToUpper() == "MKRKSERVER")
            {
                cmdText = 
                    @"select O.proj_id_||';'||O.obj_id_ as entityId 
                      from  objects O 
                      where (O.last_time_ > @maxdate) 
                         or (O.last_time_ is null)";
            }

            if (entity.ToUpper() == "MKRKSERVER")
            {
                FbConnection conn = new FbConnection(this.ServerSettings.ServiceConnectionString);
                FbCommand data = new FbCommand(cmdText, conn);
                data.Parameters.Add("@maxdate", SqlDbType.DateTime2);
                data.Parameters["@maxdate"].Value = maxdate;
                conn.Open();
                FbDataReader reader = data.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        idlist.Add(reader.SafeGetString(0));
                    }
                }

                reader.Close();
                conn.Close();
            }
            else
            {
                if (this.ServerSettings.ServiceProvider.ToUpper() == "MSSQL")
                {
                    SqlConnection conn = new SqlConnection(this.ServerSettings.ServiceConnectionString);
                    SqlCommand data = new SqlCommand(cmdText, conn);
                    data.Parameters.Add("@maxdate", SqlDbType.DateTime2);
                    data.Parameters["@maxdate"].Value = maxdate;
                    conn.Open();
                    SqlDataReader reader = data.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            idlist.Add(reader.SafeGetString(0));
                        }
                    }

                    reader.Close();
                    conn.Close();
                }

                if (this.ServerSettings.ServiceProvider.ToUpper() == "FIREBIRD")
                {
                    FbConnection conn = new FbConnection(this.ServerSettings.ServiceConnectionString);
                    FbCommand data = new FbCommand(cmdText, conn);
                    data.Parameters.Add("@maxdate", SqlDbType.DateTime2);
                    data.Parameters["@maxdate"].Value = maxdate;
                    conn.Open();
                    FbDataReader reader = data.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            idlist.Add(reader.SafeGetString(0));
                        }
                    }

                    reader.Close();
                    conn.Close();
                }
            }

            return idlist;
        }

        public ProtocolEntity GetProtocolEntity(string entityId)
        {
            //по ИД протокола вытаскивает всю сущность протокола со всеми связанными таблицами
            ProtocolEntity protocolEntity = new ProtocolEntity();
            string sqlString = "";
            string sqlStringBand = "";
            string sqlStringData = "";
            string sqlStringEmiss = "";
            string sqlStringEmp = "";
            string sqlStringFreq = "";
            string sqlStringRes = "";
            string sqlStringStrength = "";
            string sqlStringAuthors = "";

            if (this.ServerSettings.ServiceProvider.ToUpper() == "MSSQL")
            {
                SqlConnection conn = new SqlConnection(this.ServerSettings.ServiceConnectionString);
                sqlString = 
                    @"select P.id_protocol, p.num_protocol, p.date_protocol, p.id_client,
                             p.id_res, p.id_metod, p.description, p.zadanie_num, 
                             p.zadanie_date, p.zayavka_nomer, p.zayavka_date, p.id_sr_izm, 
                             p.id_rk_equipmentset, p.id_protocol_type, p.numattach, p.id_rk_type_narush,
                             p.id_measur_condition, p.departcount, p.manhour,  p.pagecount,
                             p.id_rk_request_task, p.id_client_dbs, p.id_res_dbs,  p.id_client_eis, 
                             p.id_res_eis, p.dt_modify, is_deleted, rpl_id_server 
                      from RK_PROTOCOLS P
                      where P.id_protocol = " + entityId;

                sqlStringBand = 
                    @"select PB.ID_RK_PRTCL_BAND, PB.FRQ_PERMITT, PB.BAND_MEASURED, PB.BAND_ALLOW, 
                             PB.BAND_POGR, PB.ID_RK_RAZMERNOST, PB.IS_NORMALLY, PB.ID_PRTCL_FRQ 
                      from RK_PRTCL_BAND PB 
                      where PB.ID_PROTOCOL = " + entityId;

                sqlStringData = ""; //не используется

                sqlStringEmiss =
                    @"select PE.ID_RK_PRTCL_EMISS, PE.LEVEL_EM, PE.BAND_EM, PE.BAND_EM_ALLOW, 
                             PE.BAND_EM_POGR, PE.IS_NORMALLY, PE.RPL_ID_SERVER
                      from RK_PRTCL_EMISS PE
                      where PE.ID_PROTOCOL = " + entityId;

                sqlStringEmp =
                    @"select PE.ID_RK_PRTCL_EMP, PE.MEASURE_DATE, PE.MEASURE_AZIMUTH, PE.MEASURE_DISTANCE, 
                             PE.MEASURE_GEO_LAT, PE.MEASURE_GEO_LONG, PE.MEASURE_LOCATION, PE.RPL_ID_SERVER
                      from RK_PRTCL_EMP PE
                      where PE.ID_PROTOCOL = " + entityId;

                sqlStringFreq =
                    @"select PF.ID_RK_PRTCL_FRQ, PF.FRQ_ALLOW_MIN, PF.FRQ_ALLOW_MAX, PF.FRQ_MEASURED, 
                             PF.FRQ_MEASURED_FROM, PF.FRQ_MEASURED_TO, PF.FRQ_PERMITT, PF.FRQ_PERMITT_FROM, 
                             PF.FRQ_PERMITT_TO, PF.FRQ_POGR, PF.FREQTYPE, PF.ID_RK_RAZMERNOST, PF.IS_NORMALLY, 
                             PF.IS_EXISTS, PF.RPL_ID_SERVER 
                      from RK_PRTCL_FRQ PF 
                      where PF.ID_PROTOCOL = " + entityId;

                sqlStringRes = 
                    @"select PR.ID_RK_PRTCL_RES, PR.CLIENT_NAME, PR.CLIENT_ADDRESS, PR.RES_FACTORY_ID, 
                             PR.RES_LOCATION, PR.RES_name, PR.RES_TYPE, PR.RES_VID_ETS, 
                             PR.RES_RADIOCLASS, PR.RES_FREQ_RESOLUTION_NUM, PR.RES_FREQ_RESOLUTION_DATE_START, PR.RES_FREQ_RESOLUTION_DATE_END,
                             PR.RES_CERTIFICATE_NUM, PR.RES_CERTIFICATE_DATE_START, PR.RES_CERTIFICATE_DATE_END, PR.RES_GEO_LAT, 
                             PR.RES_GEO_LONG, PR.RES_ANTENNAHEIGHT, PR.RES_POWER, PR.GEO_LAT_DELTA, 
                             PR.GEO_LAT_PERMIT, PR.GEO_LONG_DELTA, pr.GEO_LONG_PERMIT, PR.ANTHEIGHT_DELTA, 
                             PR.ANTHEIGHT_PERMIT, PR.NAME_TYPE_RADIOBUTTON, PR.MANUAL_BAND_DELTA, PR.MANUAL_BAND_DELTATYPE, 
                             PR.MANUAL_FREQ_DELTA, PR.MANUAL_FREQ_DELTATYPE, PR.RPL_ID_SERVER 
                      from RK_PRTCL_RES PR 
                      where PR.ID_PROTOCOL = " + entityId;

                sqlStringStrength =
                    @"select PS.ID_RK_PRTCL_STRENGTH, PS.STRENGTH, PS.STRENGTH_NORM, PS.FRQ_MEAS_MIN, 
                             PS.FRQ_MEAS_MAX, PS.ID_RK_RAZMERNOST, PS.IS_NORMALLY, PS.RPL_ID_SERVER
                      from RK_PRTCL_STRENGTH PS
                      where PS.ID_PROTOCOL = " + entityId;

                sqlStringAuthors = 
                    @"select S.id_rk_prtcl_sotrudnik , S.id_sotrudnik, S.RPL_ID_SERVER 
                      from rk_prtcl_sotrudnik S 
                      where S.id_protocol = " + entityId;

                SqlCommand data = new SqlCommand(sqlString, conn);

                conn.Open();
                SqlDataReader reader = data.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        protocolEntity.Id = reader.GetDecimal(0);
                        protocolEntity.Number = reader.SafeGetString(1);
                        protocolEntity.Date = reader.SafeGetDateTime(2);
                        protocolEntity.IdClient = reader.SafeGetDecimal(3).ToString();
                        protocolEntity.IdRes = reader.SafeGetDecimal(4).ToString();
                        protocolEntity.IdMethodic = reader.SafeGetDecimal(5).ToString();
                        protocolEntity.Description = reader.SafeGetString(6);
                        protocolEntity.TaskNumber = reader.SafeGetString(7);
                        protocolEntity.TaskDate = reader.SafeGetDateTime(8);
                        protocolEntity.IssueNumber = reader.SafeGetString(9);
                        protocolEntity.IssueDate = reader.SafeGetDateTime(10);
                        protocolEntity.IdRko = reader.SafeGetDecimal(11).ToString();
                        protocolEntity.IdEquipmentset = reader.SafeGetDecimal(12).ToString();
                        protocolEntity.IdProtocolType = reader.SafeGetDecimal(13).ToString();
                        protocolEntity.AttachmentCount = reader.SafeGetInt16(14).ToString();
                        protocolEntity.IdViolationType = reader.SafeGetDecimal(15).ToString();
                        protocolEntity.IdMeasureCondition = reader.SafeGetDecimal(16).ToString();
                        protocolEntity.DepartureCount = reader.SafeGetInt32(17).ToString();
                        protocolEntity.ManHours = reader.SafeGetString(18);
                        protocolEntity.PageCount = reader.SafeGetInt16(19).ToString();
                        protocolEntity.IdRequestTask = reader.SafeGetDecimal(20).ToString();
                        protocolEntity.IdClientDBS = reader.SafeGetDecimal(21).ToString();
                        protocolEntity.IdResDBS = reader.SafeGetDecimal(22).ToString();
                        protocolEntity.IdClientEIS = reader.SafeGetDecimal(23).ToString();
                        protocolEntity.IdResEIS = reader.SafeGetDecimal(24).ToString();
                        protocolEntity.DtModify = reader.SafeGetDateTime(25);
                        protocolEntity.IsDeleted = reader.SafeGetInt16(26);
                        protocolEntity.RplIdServer = reader.SafeGetString(27);
                        protocolEntity.IdRequestRes = "";
                        protocolEntity.IdRequestFreqZone = "";
                        protocolEntity.IdRequestFreq = "";
                        protocolEntity.IdRequestRec = "";
                        protocolEntity.IdRequestSanct = "";
                    }
                }

                reader.Close();
                data.CommandText = sqlStringBand;
                reader = data.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        CloudProtocolBand protocolBand = protocolEntity.CreateBand();
                        /*                sqlStringBand = "select PB.ID_RK_PRTCL_BAND, PB.FRQ_PERMITT, PB.BAND_MEASURED, PB.BAND_ALLOW, PB.BAND_POGR "+
                                                        ", PB.ID_RK_RAZMERNOST, PB.IS_NORMALLY, PB.ID_PRTCL_FRQ " +
                                                        " from RK_PRTCL_BAND PB " +
                                                        " where PB.ID_PROTOCOL = "+entityId;*/
                        protocolBand.Id = reader.SafeGetString(0);
                        protocolBand.PermitFreq = reader.SafeGetString(1);
                        protocolBand.MeasuredBand = reader.SafeGetString(2);
                        protocolBand.BandAllowed = reader.SafeGetString(3);
                        protocolBand.BandDelta = reader.SafeGetString(4);
                        protocolBand.IdMeasureLevel = reader.SafeGetString(5);
                        protocolBand.IsValid = reader.SafeGetBool(6);
                        protocolBand.IdFreq = reader.SafeGetString(7);
                        protocolBand.RplIdServer = reader.SafeGetString(8);
                        protocolEntity.AddBand(protocolBand);
                    }
                }

                reader.Close();
                data.CommandText = sqlStringEmiss;
                reader = data.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        CloudProtocolEmiss protocolEmiss = protocolEntity.CreatEmiss();
                        //"select PE.ID_RK_PRTCL_EMISS, PE.LEVEL_EM, PE.BAND_EM, PE.BAND_EM_ALLOW, PE.BAND_EM_POGR, PE.IS_NORMALLY" +
                        //" from RK_PRTCL_EMISS PE" +
                        //" where PE.ID_PROTOCOL = " + entityId;
                        protocolEmiss.Id = reader.SafeGetString(0);
                        protocolEmiss.EmissLevel = reader.SafeGetInt32(1);
                        protocolEmiss.EmissBand = reader.SafeGetString(2);
                        protocolEmiss.EmissBandAllow = reader.SafeGetString(3);
                        protocolEmiss.EmissBandDelta = reader.SafeGetString(4);
                        protocolEmiss.IsValid = reader.SafeGetBool(5);
                        protocolEmiss.RplIdServer = reader.SafeGetString(6);
                        protocolEntity.AddEmiss(protocolEmiss);
                    }
                }

                reader.Close();
                data.CommandText = sqlStringEmp;
                reader = data.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        CloudProtocolEmp protocolEmp = protocolEntity.CreatEmp();
                        //sqlStringEmp =
                        //    "select PE.ID_RK_PRTCL_EMP, PE.MEASURE_DATE, PE.MEASURE_AZIMUTH, PE.MEASURE_DISTANCE, PE.MEASURE_GEO_LAT, PE.MEASURE_GEO_LONG, PE.MEASURE_LOCATION" +
                        //    " from RK_PRTCL_EMP PE" +
                        //    " where PE.ID_PROTOCOL = " + entityId;
                        protocolEmp.Id = reader.SafeGetString(0);
                        protocolEmp.MeasureDate = reader.SafeGetDateTime(1);
                        protocolEmp.MeasureAzimuth = reader.SafeGetString(2);
                        protocolEmp.MeasureDistance = reader.SafeGetString(3);
                        protocolEmp.MeasureGeoLat = reader.SafeGetString(4);
                        protocolEmp.MeasureGeoLong = reader.SafeGetString(5);
                        protocolEmp.MeasureLocation = reader.SafeGetString(6);
                        protocolEmp.RplIdServer = reader.SafeGetString(7);
                        protocolEntity.AddEmp(protocolEmp);
                    }
                }

                reader.Close();
                data.CommandText = sqlStringFreq;
                reader = data.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        CloudProtocolFreq protocolFreq = protocolEntity.CreateFreq();
                        //"select PF.ID_RK_PRTCL_FRQ, PF.FRQ_ALLOW_MIN, PF.FRQ_ALLOW_MAX, PF.FRQ_MEASURED, PF.FRQ_MEASURED_FROM, PF.FRQ_MEASURED_TO" +
                        //", PF.FRQ_PERMITT, PF.FRQ_PERMITT_FROM, PF.FRQ_PERMITT_TO, PF.FRQ_POGR, PF.FREQTYPE, PF.ID_RK_RAZMERNOST, PF.IS_NORMALLY, PF.IS_EXISTS " +
                        //"from RK_PRTCL_FRQ PF " +
                        //"where PF.ID_PROTOCOL = " + entityId;
                        protocolFreq.Id = reader.SafeGetString(0);
                        protocolFreq.FreqAllowMin = reader.SafeGetString(1);
                        protocolFreq.FreqAllowMax = reader.SafeGetString(2);
                        protocolFreq.MeasuredFreq = reader.SafeGetString(3);
                        protocolFreq.FreqMeasuredFrom = reader.SafeGetString(4);
                        protocolFreq.FreqMeasuredTo = reader.SafeGetString(5);
                        protocolFreq.PermitFreq = reader.SafeGetString(6);
                        protocolFreq.FreqPermitFrom = reader.SafeGetString(7);
                        protocolFreq.FreqPermitTo = reader.SafeGetString(8);
                        protocolFreq.FreqDelta = reader.SafeGetString(9);
                        protocolFreq.IdMeasureLevel = reader.SafeGetString(10);
                        protocolFreq.IsValid = reader.SafeGetBool(11);
                        protocolFreq.IsExists = reader.SafeGetBool(12);
                        protocolFreq.RplIdServer = reader.SafeGetString(13);
                        protocolEntity.AddFreq(protocolFreq);
                    }
                }

                reader.Close();
                data.CommandText = sqlStringRes;
                reader = data.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        CloudProtocolRes protocolRes = protocolEntity.CreateRes();
                        //sqlStringRes = "select PR.ID_RK_PRTCL_RES, PR.CLIENT_NAME, PR.CLIENT_ADDRESS, PR.RES_FACTORY_ID, PR.RES_LOCATION, PR.RES_name, PR.RES_TYPE, " +
                        //       "PR.RES_VID_ETS, PR.RES_RADIOCLASS, PR.RES_FREQ_RESOLUTION_NUM, PR.RES_FREQ_RESOLUTION_DATE_START, PR.RES_FREQ_RESOLUTION_DATE_END," +
                        //       "PR.RES_CERTIFICATE_NUM, PR.RES_CERTIFICATE_DATE_START, PR.RES_CERTIFICATE_DATE_END, PR.RES_GEO_LAT, PR.RES_GEO_LONG, PR.RES_ANTENNAHEIGHT," +
                        //       "PR.RES_POWER, PR.GEO_LAT_DELTA, PR.GEO_LAT_PERMIT, PR.GEO_LONG_DELTA, pr.GEO_LONG_PERMIT, PR.ANTHEIGHT_DELTA, PR.ANTHEIGHT_PERMIT," +
                        //       "PR.NAME_TYPE_RADIOBUTTON, PR.MANUAL_BAND_DELTA, PR.MANUAL_BAND_DELTATYPE, PR.MANUAL_FREQ_DELTA, PR.MANUAL_FREQ_DELTATYPE " +
                        //       " from RK_PRTCL_RES PR " +
                        //       " where PR.ID_PROTOCOL = " + entityId;
                        protocolRes.Id = reader.SafeGetString(0);
                        protocolRes.ClientName = reader.SafeGetString(1);
                        protocolRes.ClientAddress = reader.SafeGetString(2);
                        protocolRes.FactoryId = reader.SafeGetString(3);
                        protocolRes.Location = reader.SafeGetString(4);
                        protocolRes.Name = reader.SafeGetString(5);
                        protocolRes.ResType = reader.SafeGetString(6);
                        protocolRes.EtsVid = reader.SafeGetString(7);
                        protocolRes.RadioClass = reader.SafeGetString(8);
                        protocolRes.FreqResolutionNumber = reader.SafeGetString(9);
                        protocolRes.FreqResolutionStartDate = reader.SafeGetDateTime(10);
                        protocolRes.FreqResolutionEndDate = reader.SafeGetDateTime(11);
                        protocolRes.CertificateNumber = reader.SafeGetString(12);
                        protocolRes.CertificateStartDate = reader.SafeGetDateTime(13);
                        protocolRes.CertificateEndDate = reader.SafeGetDateTime(14);
                        protocolRes.GeoLat = reader.SafeGetString(15);
                        protocolRes.GeoLong = reader.SafeGetString(16);
                        protocolRes.AntennaHeight = reader.SafeGetDouble(17);
                        protocolRes.Power = reader.SafeGetDouble(18);
                        protocolRes.GeoLatDelta = reader.SafeGetString(19);
                        protocolRes.GeoLatPermit = reader.SafeGetString(20);
                        protocolRes.GeoLongDelta = reader.SafeGetString(21);
                        protocolRes.GeoLongPermit = reader.SafeGetString(22);
                        protocolRes.AntennaHeightDelta = reader.SafeGetDouble(23);
                        protocolRes.AntennaHeightPermit = reader.SafeGetDouble(24);
                        protocolRes.VisualTypeSwitch = reader.SafeGetInt16(25);
                        protocolRes.ManualBandDelta = reader.SafeGetDouble(26);
                        protocolRes.ManualBandDeltaType = reader.SafeGetInt32(27);
                        protocolRes.ManualFreqDelta = reader.SafeGetDouble(28);
                        protocolRes.ManualFreqDeltaType = reader.SafeGetInt32(29);
                        protocolRes.RplIdServer = reader.SafeGetString(30);
                        protocolEntity.Res = protocolRes;
                    }
                }

                reader.Close();
                data.CommandText = sqlStringStrength;
                reader = data.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        CloudProtocolStrength protocolStrength = protocolEntity.CreateStrength();
                        //"select PS.ID_RK_PRTCL_STRENGTH, PS.STRENGTH, PS.STRENGTH_NORM, PS.FRQ_MEAS_MIN, PS.FRQ_MEAS_MAX, PS.ID_RK_RAZMERNOST," +
                        //" PS.IS_NORMALLY" +
                        //" from  RK_PRTCL_STRENGTH PS" +
                        //" where PS.ID_PROTOCOL = " + entityId;
                        protocolStrength.Id = reader.SafeGetString(0);
                        protocolStrength.Strength = reader.SafeGetString(1);
                        protocolStrength.StrengthNormal = reader.SafeGetString(2);
                        protocolStrength.FreqMeasureMin = reader.SafeGetString(3);
                        protocolStrength.FreqMeasureMax = reader.SafeGetString(4);
                        protocolStrength.IdMeasureLevel = reader.SafeGetString(5);
                        protocolStrength.IsValid = reader.SafeGetBool(6);
                        protocolStrength.RplIdServer = reader.SafeGetString(7);
                        protocolEntity.AddStrength(protocolStrength);
                    }
                }

                reader.Close();
                data.CommandText = sqlStringAuthors;
                reader = data.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        CLoudProtocolAuthors protocolAuthors = protocolEntity.CreateProtocolAuthor();
                        protocolAuthors.Id = reader.SafeGetString(0);
                        protocolAuthors.IdAuthor = reader.SafeGetString(1);
                        protocolAuthors.RplIdServer = reader.SafeGetString(2);
                        protocolEntity.AddProtocolAuthor(protocolAuthors);
                    }
                }

                conn.Close();
            }

            if (this.ServerSettings.ServiceProvider.ToUpper() == "FIREBIRD")
            {
                FbConnection conn = new FbConnection(this.ServerSettings.ServiceConnectionString);
                sqlString = 
                    @"select P.id_protocol, p.num_protocol, p.date_protocol, p.id_client,      
                             p.id_res, p.id_metod, p.description, p.zadanie_num, 
                             p.zadanie_date, p.zayavka_nomer, p.zayavka_date, p.id_sr_izm, 
                             p.id_rk_equipmentset, p.id_protocol_type, p.numattach, p.id_rk_type_narush,
                             p.id_measur_condition, p.departcount, p.manhour,  p.pagecount,
                             p.id_rk_request_task, p.id_client_dbs, p.id_res_dbs, p.id_client_eis, 
                             p.id_res_eis, p.id_rk_request_res, p.id_rk_request_frq_zone, p.id_rk_request_freq, 
                             p.id_rk_request_rec, p.id_rk_request_sanct, p.dt_modify, is_deleted, 
                             rpl_id_server                       
                      from RES_PROTOCOL_IZM_TP P
                      where P.id_protocol = " + entityId;

                sqlStringBand = 
                    @"select PB.ID_RK_PRTCL_BAND, PB.FRQ_PERMITT, PB.BAND_MEASURED, PB.BAND_ALLOW, 
                             PB.BAND_POGR, PB.ID_RK_RAZMERNOST, PB.IS_NORMALLY, PB.ID_PRTCL_FRQ, 
                             PB.RPL_ID_SERVER 
                      from RK_PRTCL_BAND PB 
                      where PB.ID_PROTOCOL = " + entityId;

                sqlStringData = 
                    @"select PD.protocol_data_id, PD.id_rk_zadacha, PD.is_auto, RPL_ID_SERVER 
                      from protocol_data PD 
                      where PD.protocol_izm_tp_id = " + entityId;

                sqlStringEmiss =
                    @"select PE.ID_RK_PRTCL_EMISS, PE.LEVEL_EM, PE.BAND_EM, PE.BAND_EM_ALLOW, 
                             PE.BAND_EM_POGR, PE.IS_NORMALLY, RPL_ID_SERVER 
                      from RK_PRTCL_EMISS PE
                      where PE.ID_PROTOCOL = " + entityId;

                sqlStringEmp =
                    @"select PE.ID_RK_PRTCL_EMP, PE.MEASURE_DATE, PE.MEASURE_AZIMUTH, PE.MEASURE_DISTANCE, 
                             PE.MEASURE_GEO_LAT, PE.MEASURE_GEO_LONG, PE.MEASURE_LOCATION, RPL_ID_SERVER 
                      from RK_PRTCL_EMP PE
                      where PE.ID_PROTOCOL = " + entityId;

                sqlStringFreq =
                    @"select PF.ID_RK_PRTCL_FRQ, PF.FRQ_ALLOW_MIN, PF.FRQ_ALLOW_MAX, PF.FRQ_MEASURED, 
                             PF.FRQ_MEASURED_FROM, PF.FRQ_MEASURED_TO, PF.FRQ_PERMITT, PF.FRQ_PERMITT_FROM, 
                             PF.FRQ_PERMITT_TO, PF.FRQ_POGR, PF.FREQTYPE, PF.ID_RK_RAZMERNOST, 
                             PF.IS_NORMALLY, PF.IS_EXISTS, RPL_ID_SERVER  
                      from RK_PRTCL_FRQ PF 
                      where PF.ID_PROTOCOL = " + entityId;

                sqlStringRes = 
                    @"select PR.ID_RK_PRTCL_RES, PR.CLIENT_NAME, PR.CLIENT_ADDRESS, PR.RES_FACTORY_ID, 
                             PR.RES_LOCATION, PR.RES_name, PR.RES_TYPE, PR.RES_VID_ETS, 
                             PR.RES_RADIOCLASS, PR.RES_FREQ_RESOLUTION_NUM, PR.RES_FREQ_RESOLUTION_DATE_START, PR.RES_FREQ_RESOLUTION_DATE_END,
                             PR.RES_CERTIFICATE_NUM, PR.RES_CERTIFICATE_DATE_START, PR.RES_CERTIFICATE_DATE_END, PR.RES_GEO_LAT, 
                             PR.RES_GEO_LONG, PR.RES_ANTENNAHEIGHT, PR.RES_POWER, PR.GEO_LAT_DELTA, 
                             PR.GEO_LAT_PERMIT, PR.GEO_LONG_DELTA, pr.GEO_LONG_PERMIT, PR.ANTHEIGHT_DELTA, 
                             PR.ANTHEIGHT_PERMIT, PR.NAME_TYPE_RADIOBUTTON, PR.MANUAL_BAND_DELTA, PR.MANUAL_BAND_DELTATYPE, 
                             PR.MANUAL_FREQ_DELTA, PR.MANUAL_FREQ_DELTATYPE, RPL_ID_SERVER  
                      from RK_PRTCL_RES PR 
                      where PR.ID_PROTOCOL = " + entityId;

                sqlStringStrength =
                    @"select PS.ID_RK_PRTCL_STRENGTH, PS.STRENGTH, PS.STRENGTH_NORM, PS.FRQ_MEAS_MIN, 
                             PS.FRQ_MEAS_MAX, PS.ID_RK_RAZMERNOST, PS.IS_NORMALLY, RPL_ID_SERVER 
                      from RK_PRTCL_STRENGTH PS
                      where PS.ID_PROTOCOL = " + entityId;

                sqlStringAuthors = 
                    @"select S.id_rk_prtcl_sotrudnik, S.id_sotrudnik, RPL_ID_SERVER 
                      from rk_prtcl_sotrudnik S 
                      where S.id_protocol = " + entityId;

                FbCommand data = new FbCommand(sqlString, conn);

                conn.Open();
                FbDataReader reader = data.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        protocolEntity.Id = reader.GetDecimal(0);
                        protocolEntity.Number = reader.SafeGetString(1);
                        protocolEntity.Date = reader.SafeGetDateTime(2);
                        protocolEntity.IdClient = reader.SafeGetDecimal(3).ToString();
                        protocolEntity.IdRes = reader.SafeGetDecimal(4).ToString();
                        protocolEntity.IdMethodic = reader.SafeGetDecimal(5).ToString();
                        protocolEntity.Description = reader.SafeGetString(6);
                        protocolEntity.TaskNumber = reader.SafeGetString(7);
                        protocolEntity.TaskDate = reader.SafeGetDateTime(8);
                        protocolEntity.IssueNumber = reader.SafeGetString(9);
                        protocolEntity.IssueDate = reader.SafeGetDateTime(10);
                        protocolEntity.IdRko = reader.SafeGetDecimal(11).ToString();
                        protocolEntity.IdEquipmentset = reader.SafeGetDecimal(12).ToString();
                        protocolEntity.IdProtocolType = reader.SafeGetDecimal(13).ToString();
                        protocolEntity.AttachmentCount = reader.SafeGetInt16(14).ToString();
                        protocolEntity.IdViolationType = reader.SafeGetDecimal(15).ToString();
                        protocolEntity.IdMeasureCondition = reader.SafeGetDecimal(16).ToString();
                        protocolEntity.DepartureCount = reader.SafeGetInt32(17).ToString();
                        protocolEntity.ManHours = reader.SafeGetString(18);
                        protocolEntity.PageCount = reader.SafeGetInt16(19).ToString();
                        protocolEntity.IdRequestTask = reader.SafeGetDecimal(20).ToString();
                        protocolEntity.IdClientDBS = reader.SafeGetDecimal(21).ToString();
                        protocolEntity.IdResDBS = reader.SafeGetDecimal(22).ToString();
                        protocolEntity.IdClientEIS = reader.SafeGetDecimal(23).ToString();
                        protocolEntity.IdResEIS = reader.SafeGetDecimal(24).ToString();
                        protocolEntity.IdRequestRes = reader.SafeGetString(25);
                        protocolEntity.IdRequestFreqZone = reader.SafeGetString(26);
                        protocolEntity.IdRequestFreq = reader.SafeGetString(27);
                        protocolEntity.IdRequestRec = reader.SafeGetString(28);
                        protocolEntity.IdRequestSanct = reader.SafeGetString(29);
                        protocolEntity.DtModify = reader.SafeGetDateTime(30);
                        protocolEntity.IsDeleted = reader.SafeGetInt16(31);
                        protocolEntity.RplIdServer = reader.SafeGetString(32);
                    }
                }

                reader.Close();
                data.CommandText = sqlStringBand;
                reader = data.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        CloudProtocolBand protocolBand = protocolEntity.CreateBand();
                        /*                sqlStringBand = "select PB.ID_RK_PRTCL_BAND, PB.FRQ_PERMITT, PB.BAND_MEASURED, PB.BAND_ALLOW, PB.BAND_POGR "+
                                                        ", PB.ID_RK_RAZMERNOST, PB.IS_NORMALLY, PB.ID_PRTCL_FRQ " +
                                                        " from RK_PRTCL_BAND PB " +
                                                        " where PB.ID_PROTOCOL = "+entityId;*/
                        protocolBand.Id = reader.SafeGetString(0);
                        protocolBand.PermitFreq = reader.SafeGetString(1);
                        protocolBand.MeasuredBand = reader.SafeGetString(2);
                        protocolBand.BandAllowed = reader.SafeGetString(3);
                        protocolBand.BandDelta = reader.SafeGetString(4);
                        protocolBand.IdMeasureLevel = reader.SafeGetString(5);
                        protocolBand.IsValid = reader.SafeGetBool(6);
                        protocolBand.IdFreq = reader.SafeGetString(7);
                        protocolBand.RplIdServer = reader.SafeGetString(8);
                        protocolEntity.AddBand(protocolBand);
                    }
                }

                reader.Close();
                data.CommandText = sqlStringEmiss;
                reader = data.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        CloudProtocolEmiss protocolEmiss = protocolEntity.CreatEmiss();
                        //"select PE.ID_RK_PRTCL_EMISS, PE.LEVEL_EM, PE.BAND_EM, PE.BAND_EM_ALLOW, PE.BAND_EM_POGR, PE.IS_NORMALLY" +
                        //" from RK_PRTCL_EMISS PE" +
                        //" where PE.ID_PROTOCOL = " + entityId;
                        protocolEmiss.Id = reader.GetString(0);
                        protocolEmiss.EmissLevel = reader.SafeGetInt32(1);
                        protocolEmiss.EmissBand = reader.SafeGetString(2);
                        protocolEmiss.EmissBandAllow = reader.SafeGetString(3);
                        protocolEmiss.EmissBandDelta = reader.SafeGetString(4);
                        protocolEmiss.IsValid = reader.SafeGetBool(5);
                        protocolEmiss.RplIdServer = reader.SafeGetString(6);
                        protocolEntity.AddEmiss(protocolEmiss);
                    }
                }

                reader.Close();
                data.CommandText = sqlStringData;
                reader = data.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        CloudProtocolData protocolData = protocolEntity.CreateProtocolData();
                        //sqlStringData = "select PD.protocol_data_id, PD.id_rk_zadacha, PD.is_auto" +
                        //                "from protocol_data PD" +
                        //                "where PD.protocol_izm_tp_id =  " + entityId;
                        protocolData.Id = reader.SafeGetString(0);
                        protocolData.IdTask = reader.SafeGetString(1);
                        protocolData.IsAuto = reader.SafeGetBool(2);
                        protocolData.RplIdServer = reader.SafeGetString(3);
                        protocolEntity.ProtocolData = protocolData;
                    }
                }

                reader.Close();
                data.CommandText = sqlStringEmp;
                reader = data.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        CloudProtocolEmp protocolEmp = protocolEntity.CreatEmp();
                        //sqlStringEmp =
                        //    "select PE.ID_RK_PRTCL_EMP, PE.MEASURE_DATE, PE.MEASURE_AZIMUTH, PE.MEASURE_DISTANCE, PE.MEASURE_GEO_LAT, PE.MEASURE_GEO_LONG, PE.MEASURE_LOCATION" +
                        //    " from RK_PRTCL_EMP PE" +
                        //    " where PE.ID_PROTOCOL = " + entityId;
                        protocolEmp.Id = reader.SafeGetString(0);
                        protocolEmp.MeasureDate = reader.SafeGetDateTime(1);
                        protocolEmp.MeasureAzimuth = reader.SafeGetString(2);
                        protocolEmp.MeasureDistance = reader.SafeGetString(3);
                        protocolEmp.MeasureGeoLat = reader.SafeGetString(4);
                        protocolEmp.MeasureGeoLong = reader.SafeGetString(5);
                        protocolEmp.MeasureLocation = reader.SafeGetString(6);
                        protocolEmp.RplIdServer = reader.SafeGetString(7);
                        protocolEntity.AddEmp(protocolEmp);
                    }
                }

                reader.Close();
                data.CommandText = sqlStringFreq;
                reader = data.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        CloudProtocolFreq protocolFreq = protocolEntity.CreateFreq();
                        //"select PF.ID_RK_PRTCL_FRQ, PF.FRQ_ALLOW_MIN, PF.FRQ_ALLOW_MAX, PF.FRQ_MEASURED, PF.FRQ_MEASURED_FROM, PF.FRQ_MEASURED_TO" +
                        //", PF.FRQ_PERMITT, PF.FRQ_PERMITT_FROM, PF.FRQ_PERMITT_TO, PF.FRQ_POGR, PF.FREQTYPE, PF.ID_RK_RAZMERNOST, PF.IS_NORMALLY, PF.IS_EXISTS " +
                        //"from RK_PRTCL_FRQ PF " +
                        //"where PF.ID_PROTOCOL = " + entityId;
                        protocolFreq.Id = reader.SafeGetString(0);
                        protocolFreq.FreqAllowMin = reader.SafeGetString(1);
                        protocolFreq.FreqAllowMax = reader.SafeGetString(2);
                        protocolFreq.MeasuredFreq = reader.SafeGetString(3);
                        protocolFreq.FreqMeasuredFrom = reader.SafeGetString(4);
                        protocolFreq.FreqMeasuredTo = reader.SafeGetString(5);
                        protocolFreq.PermitFreq = reader.SafeGetString(6);
                        protocolFreq.FreqPermitFrom = reader.SafeGetString(7);
                        protocolFreq.FreqPermitTo = reader.SafeGetString(8);
                        protocolFreq.FreqDelta = reader.SafeGetString(9);
                        protocolFreq.IdMeasureLevel = reader.SafeGetString(10);
                        protocolFreq.IsValid = reader.SafeGetBool(11);
                        protocolFreq.IsExists = reader.SafeGetBool(12);
                        protocolFreq.RplIdServer = reader.SafeGetString(13);
                        protocolEntity.AddFreq(protocolFreq);
                    }
                }

                reader.Close();
                data.CommandText = sqlStringRes;
                reader = data.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        CloudProtocolRes protocolRes = protocolEntity.CreateRes();
                        //sqlStringRes = "select PR.ID_RK_PRTCL_RES, PR.CLIENT_NAME, PR.CLIENT_ADDRESS, PR.RES_FACTORY_ID, PR.RES_LOCATION, PR.RES_name, PR.RES_TYPE, " +
                        //       "PR.RES_VID_ETS, PR.RES_RADIOCLASS, PR.RES_FREQ_RESOLUTION_NUM, PR.RES_FREQ_RESOLUTION_DATE_START, PR.RES_FREQ_RESOLUTION_DATE_END," +
                        //       "PR.RES_CERTIFICATE_NUM, PR.RES_CERTIFICATE_DATE_START, PR.RES_CERTIFICATE_DATE_END, PR.RES_GEO_LAT, PR.RES_GEO_LONG, PR.RES_ANTENNAHEIGHT," +
                        //       "PR.RES_POWER, PR.GEO_LAT_DELTA, PR.GEO_LAT_PERMIT, PR.GEO_LONG_DELTA, pr.GEO_LONG_PERMIT, PR.ANTHEIGHT_DELTA, PR.ANTHEIGHT_PERMIT," +
                        //       "PR.NAME_TYPE_RADIOBUTTON, PR.MANUAL_BAND_DELTA, PR.MANUAL_BAND_DELTATYPE, PR.MANUAL_FREQ_DELTA, PR.MANUAL_FREQ_DELTATYPE " +
                        //       " from RK_PRTCL_RES PR " +
                        //       " where PR.ID_PROTOCOL = " + entityId;
                        protocolRes.Id = reader.SafeGetString(0);
                        protocolRes.ClientName = reader.SafeGetString(1);
                        protocolRes.ClientAddress = reader.SafeGetString(2);
                        protocolRes.FactoryId = reader.SafeGetString(3);
                        protocolRes.Location = reader.SafeGetString(4);
                        protocolRes.Name = reader.SafeGetString(5);
                        protocolRes.ResType = reader.SafeGetString(6);
                        protocolRes.EtsVid = reader.SafeGetString(7);
                        protocolRes.RadioClass = reader.SafeGetString(8);
                        protocolRes.FreqResolutionNumber = reader.SafeGetString(9);
                        protocolRes.FreqResolutionStartDate = reader.SafeGetDateTime(10);
                        protocolRes.FreqResolutionEndDate = reader.SafeGetDateTime(11);
                        protocolRes.CertificateNumber = reader.SafeGetString(12);
                        protocolRes.CertificateStartDate = reader.SafeGetDateTime(13);
                        protocolRes.CertificateEndDate = reader.SafeGetDateTime(14);
                        protocolRes.GeoLat = reader.SafeGetString(15);
                        protocolRes.GeoLong = reader.SafeGetString(16);
                        protocolRes.AntennaHeight = reader.SafeGetDouble(17);
                        protocolRes.Power = reader.SafeGetDouble(18);
                        protocolRes.GeoLatDelta = reader.SafeGetString(19);
                        protocolRes.GeoLatPermit = reader.SafeGetString(20);
                        protocolRes.GeoLongDelta = reader.SafeGetString(21);
                        protocolRes.GeoLongPermit = reader.SafeGetString(22);
                        protocolRes.AntennaHeightDelta = reader.SafeGetDouble(23);
                        protocolRes.AntennaHeightPermit = reader.SafeGetDouble(24);
                        protocolRes.VisualTypeSwitch = reader.SafeGetInt16(25);
                        protocolRes.ManualBandDelta = reader.SafeGetDouble(26);
                        protocolRes.ManualBandDeltaType = reader.SafeGetInt32(27);
                        protocolRes.ManualFreqDelta = reader.SafeGetDouble(28);
                        protocolRes.ManualFreqDeltaType = reader.SafeGetInt32(29);
                        protocolRes.RplIdServer = reader.SafeGetString(30);
                        protocolEntity.Res = protocolRes;
                    }
                }

                reader.Close();
                data.CommandText = sqlStringStrength;
                reader = data.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        CloudProtocolStrength protocolStrength = protocolEntity.CreateStrength();
                        //"select PS.ID_RK_PRTCL_STRENGTH, PS.STRENGTH, PS.STRENGTH_NORM, PS.FRQ_MEAS_MIN, PS.FRQ_MEAS_MAX, PS.ID_RK_RAZMERNOST," +
                        //" PS.IS_NORMALLY" +
                        //" from  RK_PRTCL_STRENGTH PS" +
                        //" where PS.ID_PROTOCOL = " + entityId;
                        protocolStrength.Id = reader.SafeGetString(0);
                        protocolStrength.Strength = reader.SafeGetString(1);
                        protocolStrength.StrengthNormal = reader.SafeGetString(2);
                        protocolStrength.FreqMeasureMin = reader.SafeGetString(3);
                        protocolStrength.FreqMeasureMax = reader.SafeGetString(4);
                        protocolStrength.IdMeasureLevel = reader.SafeGetString(5);
                        protocolStrength.IsValid = reader.SafeGetBool(6);
                        protocolStrength.RplIdServer = reader.SafeGetString(7);
                        protocolEntity.AddStrength(protocolStrength);
                    }
                }

                reader.Close();
                data.CommandText = sqlStringAuthors;
                reader = data.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        CLoudProtocolAuthors protocolAuthors = protocolEntity.CreateProtocolAuthor();
                        protocolAuthors.Id = reader.SafeGetString(0);
                        protocolAuthors.IdAuthor = reader.SafeGetString(1);
                        protocolAuthors.RplIdServer = reader.SafeGetString(2);
                        protocolEntity.AddProtocolAuthor(protocolAuthors);
                    }
                }

                conn.Close();
            }

            return protocolEntity;
        }

        public ActEntity GetActEntity(string entityId)
        {
            ActEntity actEntity = new ActEntity();
            string sqlStringAct;
            string sqlStringProtocols = "";
            string sqlStringActPerformers = "";
            string sqlStringRKO = "";
            string sqlStringWorkTimes = "";
            string sqlStringTasks = "";
            string sqlStringActStatuses = "";
            string sqlStringActAuthors = "";
            string sqlStringActErrorClasses = "";
            string sqlStringActRequestDetail = "";

            if (this.ServerSettings.ServiceProvider.ToUpper() == "MSSQL")
            {

            }

            if (this.ServerSettings.ServiceProvider.ToUpper() == "FIREBIRD")
            {
                sqlStringAct = 
                    @"select a.act_num, a.act_date, a.id_sp_activity, a.osnovanie,  
                             a.vyvod, a.location, a.protocol_data_string, a.id_man_confirm,  
                             a.departcount, a.manhour, a.id_rk_request_task, a.dt_modify, 
                             a.rpl_id_server 
                      from act_data A 
                      where A.id_act_data = " + entityId;

                sqlStringProtocols = 
                    @"select RAP.id_rel_act_protocol, rap.id_protocol, rap.rpl_id_server 
                      from rk_rel_act_protocol RAP 
                      where RAP.id_act_data = " + entityId;

                sqlStringActPerformers = 
                    @"select P.id_act_performer, p.id_sotrudnik, p.rpl_id_server
                      from act_performer P 
                      where P.id_act_data = " + entityId;

                sqlStringRKO = 
                    @"select R.id_act_sr_izm, R.id_sr_izm, R.rpl_id_server 
                      from act_sr_izm R 
                      where  R.id_act_data =" + entityId;

                sqlStringWorkTimes = 
                    @"select W.id_act_work_time, w.dtbegin, w.dtend 
                      from act_work_time W 
                      where W.id_act_data = " + entityId;

                sqlStringTasks = 
                    @"select az.id_rel_rk_act_zadacha, az.id_rk_zadacha, az.rpl_id_server 
                      from rel_rk_act_zadacha az 
                      where az.id_act_data = " + entityId;

                sqlStringActStatuses = 
                    @"select st.id_rel_act_status, st.id_act_status, st.id_sotrudnik, st.status_date, 
                             st.msg, st.rpl_id_server 
                      from rk_rel_act_statuses st 
                      where st.id_act_data = " + entityId;

                sqlStringActAuthors = 
                    @"select a.id_rel_act_author, a.id_sotrudnik, a.rpl_id_server 
                      from rk_rel_act_authors a 
                      where a.id_act_data = " + entityId;

                sqlStringActErrorClasses = 
                    @"select ec.id_act_error_class, ec.classification, ec.rpl_id_server 
                      from rk_rel_act_error_class ec 
                      where ec.id_act_data = " + entityId;

                sqlStringActRequestDetail = 
                    @"select ad.id_rel_act_request_details, ad.request_table_name, ad.request_table_id_key, ad.rpl_id_server 
                      from rk_rel_act_request_details ad 
                      where ad.id_act_data = " + entityId;

                FbConnection conn = new FbConnection(this.ServerSettings.ServiceConnectionString);
                FbCommand data = new FbCommand(sqlStringAct, conn);
                conn.Open();
                FbDataReader reader = data.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        //"select " +
                        //                              "  a.act_num, a.act_date, a.id_sp_activity, a.osnovanie,  " +
                        //                              "  a.vyvod,  a.location, " +
                        //                              " a.protocol_data_string, a.id_man_confirm,  " +
                        //                              " a.departcount, " +
                        //                              " a.manhour, a.id_rk_request_task " +
                        //                              " from act_data A " +
                        //                              " where A.id_act_data = " + entityId;
                        actEntity.Id = entityId;
                        actEntity.Number = reader.SafeGetString(0);
                        actEntity.Date = reader.SafeGetDateTime(1);
                        actEntity.IdActivity = reader.SafeGetString(2);
                        actEntity.ConditionAsText = reader.SafeGetString(3);
                        actEntity.Conclusion = reader.SafeGetString(4);
                        actEntity.Location = reader.SafeGetString(5);
                        actEntity.ProtocolsAsText = reader.SafeGetString(6);
                        actEntity.IdConfirmer = reader.SafeGetString(7);
                        actEntity.DepartureCount = reader.SafeGetInt16(8);
                        actEntity.ManHours = reader.SafeGetDouble(9);
                        actEntity.IdRequestTasks = reader.SafeGetString(10);
                        actEntity.DtModify = reader.SafeGetDateTime(11);
                        actEntity.RplIdServer = reader.SafeGetString(12);
                    }
                }

                reader.Close();
                data.CommandText = sqlStringProtocols;
                reader = data.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        CloudRel protocol = new CloudRel();
                        protocol.Id = reader.SafeGetString(0);//id связки
                        protocol.ForeignKeyId = reader.SafeGetString(1);//id протокола
                        protocol.RplIdServer = reader.SafeGetString(2);
                        actEntity.AddProtocol(protocol);
                    }
                }

                reader.Close();
                data.CommandText = sqlStringActPerformers;
                reader = data.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        CloudRel actPerformer = new CloudRel();
                        actPerformer.Id = reader.SafeGetString(0);
                        actPerformer.ForeignKeyId = reader.SafeGetString(1);
                        actPerformer.RplIdServer = reader.SafeGetString(2);
                        actEntity.AddPerformer(actPerformer);
                    }
                }

                reader.Close();
                data.CommandText = sqlStringRKO;
                reader = data.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        CloudRel rko = new CloudRel();
                        rko.Id = reader.SafeGetString(0);//id связки
                        rko.ForeignKeyId = reader.SafeGetString(1);//id рко
                        actEntity.AddRko(rko);
                    }
                }

                reader.Close();
                data.CommandText = sqlStringWorkTimes;
                reader = data.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        //"select " +
                        //                                     " W.id_act_work_time,  w.dtbegin, w.dtend " +
                        //                                     " from act_work_time W " +
                        //                                     " where    W.id_act_data = " + entityId;
                        CloudActWorkTime workTime = new CloudActWorkTime();
                        workTime.Id = reader.SafeGetString(0);
                        workTime.DateFrom = reader.SafeGetDateTime(1);
                        workTime.DateTo = reader.SafeGetDateTime(2);
                        actEntity.AddWorkTime(workTime);
                    }
                }

                reader.Close();
                data.CommandText = sqlStringTasks;
                reader = data.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        CloudRel task = new CloudRel();
                        task.Id = reader.SafeGetString(0);
                        task.ForeignKeyId = reader.SafeGetString(1);
                        task.RplIdServer = reader.SafeGetString(2);
                        actEntity.AddTask(task);
                    }
                }

                reader.Close();
                data.CommandText = sqlStringActStatuses;
                reader = data.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        //"select " +
                        //                                       " st.id_rel_act_status, st.id_act_status, st.id_sotrudnik, st.status_date, st.msg " +
                        //                                       " from rk_rel_act_statuses st " +
                        //                                       " where " +
                        //                                       " st.id_act_data = " + entityId;
                        CloudActStatus actStatus = new CloudActStatus();
                        actStatus.Id = reader.SafeGetString(0);
                        actStatus.IdStatus = reader.SafeGetString(1);
                        actStatus.IdSotrudnik = reader.SafeGetString(2);
                        actStatus.StatusDate = reader.SafeGetDateTime(3);
                        actStatus.Message = reader.SafeGetString(4);
                        actStatus.RplIdServer = reader.SafeGetString(5);
                        actEntity.AddActStatus(actStatus);
                    }
                }

                reader.Close();
                data.CommandText = sqlStringActAuthors;
                reader = data.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        CloudRel actAuthor = new CloudRel();
                        actAuthor.Id = reader.SafeGetString(0);
                        actAuthor.ForeignKeyId = reader.SafeGetString(1);
                        actAuthor.RplIdServer = reader.SafeGetString(2);
                        actEntity.AddActAuthor(actAuthor);
                    }
                }

                reader.Close();
                data.CommandText = sqlStringActErrorClasses;
                reader = data.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        //"select " +
                        //                                          " ec.id_act_error_class, ec.classification " +
                        //                                          " from rk_rel_act_error_class ec " +
                        //                                          " where " +
                        //                                          " ec.id_act_data = " + entityId;
                        CloudActErrorClass errorClass = new CloudActErrorClass();
                        errorClass.Id = reader.SafeGetString(0);
                        errorClass.ErrorClass = reader.SafeGetString(1);
                        errorClass.RplIdServer = reader.SafeGetString(2);
                        actEntity.AddActErrorClass(errorClass);
                    }
                }

                reader.Close();
                data.CommandText = sqlStringActRequestDetail;
                reader = data.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        //= "select " +
                        //                                           " ad.id_rel_act_request_details, ad.request_table_name, ad.request_table_id_key " +
                        //                                           " from rk_rel_act_request_details ad " +
                        //                                           " where " +
                        //                                           " ad.id_act_data = " + entityId;
                        CloudActRequestDetails requestDetail = new CloudActRequestDetails();
                        requestDetail.Id = reader.SafeGetString(0);
                        requestDetail.TableName = reader.SafeGetString(1);
                        requestDetail.TableKeyValue = reader.SafeGetString(2);
                        requestDetail.RplIdServer = reader.SafeGetString(3);
                        actEntity.AddActRequestDetail(requestDetail);
                    }
                }

                conn.Close();
            }

            return actEntity;
        }

        public RkoEntity GetOldSchemaRkoEntity(string entityId)
        {
            RkoEntity rkoEntity = new RkoEntity();
            string sqlStringRko;
            string sqlStringActRko;
            string sqlStringRkoMetrology;

            if (this.ServerSettings.ServiceProvider.ToUpper() == "MSSQL")
            {

            }

            if (this.ServerSettings.ServiceProvider.ToUpper() == "FIREBIRD")
            {
                sqlStringRko = 
                    @"select R.id_sr_izm, S.name_sr, R.dat_vvod, R.zavod_num, 
                             R.ipaddr, R.inv_num, K.shirota, K.dolgota, 
                             R.dt_modify, coalesce(S.izgotovit, R.izgotovit) izgotovit,
                             A.full_address, P.id_rkp, P1.id_podrazdelenie, P1.podrazdname, 
                             P1.is_rkp, P2.id_region, P2.id_podrazdelenie id_podrazdelenie2, P2.podrazdname podrazdname2,  
                             P.rpl_id_server post_rpl_id_server, P2.rpl_id_server subdivision_rpl_id_server, R.rpl_id_server rpl_id_server 
                      from RK_SR_IZM R 
                             inner join sp_sredstv S on R.id_sp_sredstv = S.id_sp_sredstv 
                             left join rk_place K on R.id_place = K.id_rk_place 
                             left join rk_address A on K.id_rk_address = A.id_rk_address 
                             left join rk_sr_podrazd P on R.id_sr_izm = P.id_sr_izm 
                             left join podrazdelenie P1 on ((P.id_rkp = P1.id_podrazdelenie)) 
                             left join podrazdelenie P2 on ((P1.id_parent = P2.id_podrazdelenie)) 
                      where (P1.is_rkp = 1) 
                        and (P2.is_rkp = 0) 
                        and (R.id_sr_izm = " + entityId + @")
                      union all 
                      select R.id_sr_izm, S.name_sr, R.dat_vvod, R.zavod_num, 
                             R.ipaddr, R.inv_num, K.shirota, K.dolgota, 
                             R.dt_modify, coalesce(S.izgotovit, R.izgotovit) izgotovit, 
                             A.full_address, P.id_rkp, P1.id_podrazdelenie, P1.podrazdname, 
                             P1.is_rkp, P2.id_region, P2.id_podrazdelenie id_podrazdelenie2, P2.podrazdname podrazdname2,  
                             P.rpl_id_server post_rpl_id_server, P2.rpl_id_server subdivision_rpl_id_server, R.rpl_id_server rpl_id_server 
                      from RK_SR_IZM R 
                             inner join sp_sredstv S on R.id_sp_sredstv = S.id_sp_sredstv 
                             left join rk_place K on R.id_place = K.id_rk_place 
                             left join rk_address A on K.id_rk_address = A.id_rk_address 
                             left join rk_sr_podrazd P on R.id_sr_izm = P.id_sr_izm 
                             left join podrazdelenie P1 on ((P.id_rkp = P1.id_podrazdelenie)) 
                             left join podrazdelenie P2 on ((P1.id_parent = P2.id_podrazdelenie)) 
                      where (P1.is_rkp = 0) 
                        and (P2.is_rkp = 0) 
                        and (R.id_sr_izm = " + entityId + ")";

                sqlStringActRko = 
                    @"select AR.id_act_sr_izm, AR.id_act_data, AR.RPL_ID_SERVER 
                      from act_sr_izm AR 
                      where AR.id_sr_izm = " + entityId;

                sqlStringRkoMetrology =
                    @"select RM.id_rko_metrology, RM.cert_num, RM.date_from, RM.date_to, 
                             RM.filename, RM.""PAGES"", RM.cert_org  
                      from rk_rko_metrology RM
                      where RM.id_rko = " + entityId;

                FbConnection conn = new FbConnection(this.ServerSettings.ServiceConnectionString);
                FbCommand data = new FbCommand(sqlStringRko, conn);
                conn.Open();
                FbDataReader reader;
                reader = data.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        rkoEntity.Id = entityId;
                        rkoEntity.RkoName = reader.SafeGetString(1);
                        rkoEntity.RkoDate = reader.SafeGetDateTime(2);
                        rkoEntity.FactoryNumber = reader.SafeGetString(3);
                        rkoEntity.IpAddress = reader.SafeGetString(4);
                        rkoEntity.InvNumber = reader.SafeGetString(5);
                        rkoEntity.GeoLat = reader.SafeGetString(6);
                        rkoEntity.GeoLong = reader.SafeGetString(7);
                        rkoEntity.DtModify = reader.SafeGetDateTime(8);
                        rkoEntity.VendorName = reader.SafeGetString(9);
                        rkoEntity.PostAddress = reader.SafeGetString(10);
                        rkoEntity.IdPost = reader.SafeGetString(11);//P.id_rkp
                        rkoEntity.PostName = reader.SafeGetString(13);// P1.podrazdname
                        rkoEntity.IsPost = reader.SafeGetString(14);
                        rkoEntity.IdRegion = reader.SafeGetString(15);
                        rkoEntity.IdSubdivision = reader.SafeGetString(16);// P2.id_podrazdelenie
                        rkoEntity.SubvisionName = reader.SafeGetString(17);// P2.podrazdname
                        rkoEntity.PostRplIdServer = reader.SafeGetString(18); // P.rpl_id_server
                        rkoEntity.SubdivisionRplIdServer = reader.SafeGetString(19); //P2.rpl_id_server
                        rkoEntity.RplIdServer = reader.SafeGetString(20); //R.rpl_id_server 
                    }
                }

                reader.Close();
                data.CommandText = sqlStringActRko;
                reader = data.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        CloudActRko actrko = new CloudActRko();
                        actrko.Id = reader.SafeGetString(0);
                        actrko.Act = reader.SafeGetString(1);
                        actrko.RplIdServer = reader.SafeGetString(2);
                        rkoEntity.AddAct(actrko);
                    }
                }

                reader.Close();
                data.CommandText = sqlStringRkoMetrology;
                reader = data.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        CloudRkoMetrology metrology = new CloudRkoMetrology();
                        metrology.Id = reader.SafeGetString(0);
                        metrology.CertNum = reader.SafeGetString(1);
                        metrology.CertDateFrom = reader.SafeGetDateTime(2);
                        metrology.CertDateTo = reader.SafeGetDateTime(3);
                        metrology.CertFileName = reader.SafeGetString(4);
                        metrology.CertPageCount = reader.SafeGetString(5);
                        metrology.CertAuthority = reader.SafeGetString(6);
                        rkoEntity.AddRkoMetrology(metrology);
                    }
                }

                reader.Close();
                conn.Close();
            }

            return rkoEntity;
        }

        public SubdivisionEntity GetSubdivisionEntity(string entityId)
        {
            SubdivisionEntity subdivisionEntity = new SubdivisionEntity();
            string sqlStringSubdivision;

            if (this.ServerSettings.ServiceProvider.ToUpper() == "MSSQL")
            {

            }

            if (this.ServerSettings.ServiceProvider.ToUpper() == "FIREBIRD")
            {
                sqlStringSubdivision = 
                    @"select P.id_podrazdelenie, P.podrazdname, P.podrazdname_sokr, P.id_region, P.id_parent 
                      from podrazdelenie P 
                      where P.id_podrazdelenie = " + entityId;

                FbConnection conn = new FbConnection(this.ServerSettings.ServiceConnectionString);
                FbCommand data = new FbCommand(sqlStringSubdivision, conn);
                conn.Open();
                FbDataReader reader = data.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        subdivisionEntity.Id = entityId;
                        subdivisionEntity.SubdivisionName = reader.SafeGetString(0);
                        subdivisionEntity.SubdivisionShortName = reader.SafeGetString(1);
                        subdivisionEntity.IdRegion = reader.SafeGetString(2);
                        subdivisionEntity.IdParent = reader.SafeGetString(3);
                    }
                }

                reader.Close();
                conn.Close();
            }

            return subdivisionEntity;
        }

        public EmployeeEntity GetEmployeeEntity(string entityId)
        {
            EmployeeEntity employeeEntity = new EmployeeEntity();
            string sqlStringEmployee;
            string sqlStringCertificates;
            if (this.ServerSettings.ServiceProvider.ToUpper() == "MSSQL")
            {

            }

            if (this.ServerSettings.ServiceProvider.ToUpper() == "FIREBIRD")
            {
                sqlStringEmployee = 
                    @"select S.famil, S.name, S.otch, S.phone, 
                             S.phone_work, S.phone_work_prn, S.email, S.id_otdel, 
                             S.l_lock, S.date_disable, S.id_dl, s.dbs_id, 
                             S.id_polz_bd, S.sotrudnik_login, S.ID_USER_CERTIFICATE, S.dt_modify, 
                             S.is_deleted, S.RPL_ID_SERVER 
                      from sotrudnik S 
                      where S.id_sotrudnik = " + entityId;

                sqlStringCertificates = 
                    @"select UC.id_user_certificate, UC.name, uc.serial_number, uc.thumbprint, uc.certificate 
                      from rk_pki_user_certificates UC 
                             inner join sotrudnik S on S.id_user_certificate = UC.id_user_certificate 
                      where S.id_sotrudnik = " + entityId;

                FbConnection conn = new FbConnection(this.ServerSettings.ServiceConnectionString);
                FbCommand data = new FbCommand(sqlStringEmployee, conn);
                conn.Open();
                FbDataReader reader = data.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        employeeEntity.Id = entityId;
                        employeeEntity.Surname = reader.SafeGetString(0);
                        employeeEntity.Name = reader.SafeGetString(1);
                        employeeEntity.SecondName = reader.SafeGetString(2);
                        employeeEntity.Phone = reader.SafeGetString(3);
                        employeeEntity.PhoneWork = reader.SafeGetString(4);
                        employeeEntity.PhoneWorkPrn = reader.SafeGetString(5);
                        employeeEntity.Email = reader.SafeGetString(6);
                        employeeEntity.IdSubdivision = reader.SafeGetString(7);
                        employeeEntity.Lock = reader.SafeGetString(8);
                        employeeEntity.DisableDate = reader.SafeGetDateTime(9);
                        employeeEntity.IdPosition = reader.SafeGetString(10);
                        employeeEntity.IdDbs = reader.SafeGetString(11);
                        employeeEntity.IdDbUser = reader.SafeGetString(12);
                        employeeEntity.Login = reader.SafeGetString(13);
                        employeeEntity.IdUserCertificate = reader.SafeGetString(14);
                        employeeEntity.DtModify = reader.SafeGetDateTime(15);
                        employeeEntity.IsDeleted = reader.SafeGetString(16);
                        employeeEntity.RplIdServer = reader.SafeGetString(17);
                    }
                }

                reader.Close();
                data.CommandText = sqlStringCertificates;
                reader = data.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        CloudEmployeeCertificate certificate = new CloudEmployeeCertificate();
                        certificate.Id = reader.SafeGetString(0);
                        certificate.Name = reader.SafeGetString(1);
                        certificate.SerialNumber = reader.SafeGetString(2);
                        certificate.ThumbPrint = reader.SafeGetString(3);
                        certificate.Certificate = reader.SafeGetString(4);
                        employeeEntity.AddCertificate(certificate);
                    }
                }

                conn.Close();
            }

            return employeeEntity;
        }

        public PlanDetailEntity GetPlanDetailEntity(string entityId)
        {
            PlanDetailEntity planDetailEntity = new PlanDetailEntity();
            string sqlStringPlanDetail;
            string sqlStringPlanDetailFreq;

            if (this.ServerSettings.ServiceProvider.ToUpper() == "MSSQL")
            {

            }

            if (this.ServerSettings.ServiceProvider.ToUpper() == "FIREBIRD")
            {
                sqlStringPlanDetail = 
                    @"select d.id_rk_plandetail, d.id_rk_plan, d.id_region, d.id_res, 
                             d.startdat, d.enddat, d.id_zadacha, d.id_protocol, 
                             d.id_act_data, d.id_sp_type_freq_task, d.freq, d.frqstart, 
                             d.frqend, d.step, d.id_rko, d.id_object, 
                             d.id_rk_razmernost, d.id_operation, d.is_mark_del, d.is_mobile, 
                             d.id_period, d.id_client, d.id_rk_pl_st_obj_per, d.id_rk_pl_mob_obj_per, 
                             d.id_rk_plan_frq_detail, d.id_rk_plan_request_obj, d.id_sotrudnik, d.id_trassa, 
                             d.id_point, d.id_sanction_l, d.id_sanction, d.id_type_in_plan, 
                             d.id_rk_mtd, d.time_control, d.is_kv, d.id_operation_kv, 
                             d.id_raspisanie, d.id_rk_request_task, d.id_client_dbs, d.id_res_dbs, 
                             d.id_client_eis, d.id_res_eis, d.start_record, d.end_record, 
                             d.title, d.id_channel, d.id_rk_plan_record, d.id_rk_plan_monitoring, 
                             d.id_rk_request_task_res, d.id_sp_type_plan_task, d.dt_modify, d.count_res, 
                             d.count_frq, d.f_string, d.request_num, d.request_date, 
                             d.rpl_id_server 
                      from rk_aggr_plandetail D 
                      where D.id_rk_aggr_plandetail = " + entityId;

                sqlStringPlanDetailFreq = 
                    @"select PF.id_rk_aggr_plandetail_frq, pf.id_rk_plan_frq_list, pf.freq, pf.freq_int, 
                             pf.freq_width, pf.freq_width_int, pf.id_rk_razmernost, pf.lofreq, 
                             pf.hifreq, pf.frq_step, pf.id_sp_band_diap, pf.id_sp_band_frq, 
                             pf.lofreq_int, pf.hifreq_int, pf.frq_step_int, pf.rpl_id_server  
                      from rk_aggr_plandetail_frq PF 
                      where pf.id_rk_aggr_plandetail = " + entityId;

                FbConnection conn = new FbConnection(this.ServerSettings.ServiceConnectionString);
                FbCommand data = new FbCommand(sqlStringPlanDetail, conn);
                conn.Open();
                FbDataReader reader = data.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        planDetailEntity.Id = entityId;
                        planDetailEntity.IdPlanDetailBd = reader.SafeGetString(0);
                        planDetailEntity.IdPlan = reader.SafeGetString(1);
                        planDetailEntity.IdRegion = reader.SafeGetString(2);
                        planDetailEntity.IdRes = reader.SafeGetString(3);
                        planDetailEntity.DtStart = reader.SafeGetDateTime(4);
                        planDetailEntity.DtEnd = reader.SafeGetDateTime(5);
                        planDetailEntity.IdZadacha = reader.SafeGetString(6);
                        planDetailEntity.IdProtocol = reader.SafeGetString(7);
                        planDetailEntity.IdAct = reader.SafeGetString(8);
                        planDetailEntity.IdFreqTask = reader.SafeGetString(9);
                        planDetailEntity.Freq = reader.SafeGetString(10);
                        planDetailEntity.FreqStart = reader.SafeGetString(11);
                        planDetailEntity.FreqEnd = reader.SafeGetString(12);
                        planDetailEntity.FreqStep = reader.SafeGetString(13);
                        planDetailEntity.IdRko = reader.SafeGetString(14);
                        planDetailEntity.IdObject = reader.SafeGetString(15);
                        planDetailEntity.IdRazm = reader.SafeGetString(16);
                        planDetailEntity.IdOperation = reader.SafeGetString(17);
                        planDetailEntity.IsMarkDel = reader.SafeGetString(18);
                        planDetailEntity.IsMobile = reader.SafeGetString(19);
                        planDetailEntity.IdPeriod = reader.SafeGetString(20);
                        planDetailEntity.IdClient = reader.SafeGetString(21);
                        planDetailEntity.IdStObjPeriod = reader.SafeGetString(22);
                        planDetailEntity.IdMobObjPeriod = reader.SafeGetString(23);
                        planDetailEntity.IdFrqDetail = reader.SafeGetString(24);
                        planDetailEntity.IdRequestObj = reader.SafeGetString(25);
                        planDetailEntity.IdSotrudnik = reader.SafeGetString(26);
                        planDetailEntity.IdTrassa = reader.SafeGetString(27);
                        planDetailEntity.IdPoint = reader.SafeGetString(28);
                        planDetailEntity.IdSanctionL = reader.SafeGetString(29);
                        planDetailEntity.IdSanction = reader.SafeGetString(30);
                        planDetailEntity.IdType = reader.SafeGetString(31);
                        planDetailEntity.IdMtd = reader.SafeGetString(32);
                        planDetailEntity.TimeControl = reader.SafeGetString(33);
                        planDetailEntity.IsKv = reader.SafeGetString(34);
                        planDetailEntity.IdOperationKv = reader.SafeGetString(35);
                        planDetailEntity.IdRaspisanie = reader.SafeGetString(36);
                        planDetailEntity.IdTask = reader.SafeGetString(37);
                        planDetailEntity.IdClientDbs = reader.SafeGetString(38);
                        planDetailEntity.IdResDbs = reader.SafeGetString(39);
                        planDetailEntity.IdClientEis = reader.SafeGetString(40);
                        planDetailEntity.IdResEis = reader.SafeGetString(41);
                        planDetailEntity.StartRec = reader.SafeGetString(42);
                        planDetailEntity.EndRec = reader.SafeGetString(43);
                        planDetailEntity.Title = reader.SafeGetString(44);
                        planDetailEntity.IdChannel = reader.SafeGetString(45);
                        planDetailEntity.IdPlanRec = reader.SafeGetString(46);
                        planDetailEntity.IdPlanMon = reader.SafeGetString(47);
                        planDetailEntity.IdTaskRes = reader.SafeGetString(48);
                        planDetailEntity.IdTypePlanTask = reader.SafeGetString(49);
                        planDetailEntity.DtModify = reader.SafeGetDateTime(50);
                        planDetailEntity.CountRes = reader.SafeGetString(51);
                        planDetailEntity.CountFrq = reader.SafeGetString(52);
                        planDetailEntity.FString = reader.SafeGetString(53);
                        planDetailEntity.NumRequest = reader.SafeGetString(54);
                        planDetailEntity.DateRequest = reader.SafeGetDateTime(55);
                        planDetailEntity.RplIdServer = reader.SafeGetString(56);
                    }
                }

                reader.Close();
                data.CommandText = sqlStringPlanDetailFreq;
                reader = data.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        CloudPlanDetailFreq planDetailFreq = new CloudPlanDetailFreq();
                        planDetailFreq.Id = reader.SafeGetString(0);
                        planDetailFreq.IdFrqList = reader.SafeGetString(1);
                        planDetailFreq.Freq = reader.SafeGetString(2);
                        planDetailFreq.FreqInt = reader.SafeGetString(3);
                        planDetailFreq.FreqWidth = reader.SafeGetString(4);
                        planDetailFreq.FreqWidthInt = reader.SafeGetString(5);
                        planDetailFreq.IdRazm = reader.SafeGetString(6);
                        planDetailFreq.FreqStart = reader.SafeGetString(7);
                        planDetailFreq.FreqEnd = reader.SafeGetString(8);
                        planDetailFreq.FreqStep = reader.SafeGetString(9);
                        planDetailFreq.IdBandDiap = reader.SafeGetString(10);
                        planDetailFreq.FreqStartInt = reader.SafeGetString(11);
                        planDetailFreq.FreqEndInt = reader.SafeGetString(12);
                        planDetailFreq.FreqStepInt = reader.SafeGetString(13);
                        planDetailFreq.RplIdServer = reader.SafeGetString(14);
                        planDetailEntity.AddFreq(planDetailFreq);
                    }
                }

                conn.Close();
            }

            return planDetailEntity;
        }

        public MkrkEntity GetMkrkEntity(string entityId)
        {
            MkrkEntity mkrkEntity = new MkrkEntity();
            string sqlStringObject = "";
            string sqlStringProject = "";
            string sqlStringData = "";

            if (this.ServerSettings.ServiceProvider.ToUpper() == "MSSQL")
            {

            }

            if (this.ServerSettings.ServiceProvider.ToUpper() == "FIREBIRD")
            {
                sqlStringObject = 
                    @"select O.id_mon_objects, o.name, o.obj_id, o.last_time, 
                             o.last_lon, o.last_lat, o.last_speed, o.last_add_info, 
                             o.proj_id, o.phone, o.id_mon_projects, o.id_rko 
                      from mon_objects O 
                      where O.id_mon_objects = " + entityId;

                sqlStringProject = 
                    @"select p.name, p.PASSWORD, p.id_region 
                      from mon_projects P 
                             inner join mon_objects O on P.id_mon_projects = O.id_mon_projects 
                      where O.id_mon_objects = " + entityId;

                sqlStringData = 
                    @"select D.id_mon_data, D.mon_time, D.lat, D.lon, 
                             D.speed, D.add_info 
                      from mon_data D 
                             inner join mon_objects O on D.id_mon_objects = O.id_mon_objects 
                      where O.id_mon_objects = " + entityId;

                FbConnection conn = new FbConnection(this.ServerSettings.ServiceConnectionString);
                FbCommand data = new FbCommand(sqlStringObject, conn);
                conn.Open();
                FbDataReader reader = data.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        mkrkEntity.Id = entityId;
                        mkrkEntity.ObjName = reader.SafeGetString(1);
                        mkrkEntity.ObjId = reader.SafeGetString(2);
                        mkrkEntity.DtModify = reader.SafeGetDateTime(3);
                        mkrkEntity.Lon = reader.SafeGetString(4);
                        mkrkEntity.Lat = reader.SafeGetString(5);
                        mkrkEntity.Speed = reader.SafeGetString(6);
                        mkrkEntity.AddInfo = reader.SafeGetString(7);
                        mkrkEntity.ProjId = reader.SafeGetString(8);
                        mkrkEntity.Phone = reader.SafeGetString(9);
                        mkrkEntity.IdProject = reader.SafeGetString(10);
                        mkrkEntity.IdRko = reader.SafeGetString(11);
                    }
                }

                reader.Close();
                data.CommandText = sqlStringProject;
                reader = data.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        mkrkEntity.ProjName = reader.SafeGetString(0);
                        mkrkEntity.Password = reader.SafeGetString(1);
                        mkrkEntity.IdRegion = reader.SafeGetString(2);
                    }
                }

                reader.Close();

                data.CommandText = sqlStringData;
                reader = data.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        MkrkData newData = new MkrkData();
                        newData.Id = reader.SafeGetString(0);
                        newData.MonTime = reader.SafeGetDateTime(1);
                        newData.Lat = reader.SafeGetString(2);
                        newData.Lon = reader.SafeGetString(3);
                        newData.Speed = reader.SafeGetString(4);
                        newData.AddInfo = reader.SafeGetString(5);
                        mkrkEntity.AddData(newData);
                    }
                }

                reader.Close();
                conn.Close();
            }

            return mkrkEntity;
        }

        public MkrkEntity GetMkrkEntityFromServerMkrk(string entityIdProjId, string entityIdObjId, DateTime dateTime)
        {
            MkrkEntity mkrkEntity = new MkrkEntity();
            string sqlStringObject = "";
            string sqlStringProject = "";
            string sqlStringData = "";

            sqlStringObject = 
                @"select NULL as id_mon_objects, O.name_, O.obj_id_, O.last_time_, 
                         O.last_lon_, O.last_lat_, O.last_speed_, O.last_add_info_, 
                         O.proj_id_, NULL as id_mon_projects  
                  from OBJECTS O  
                  where O.obj_id_ = " + entityIdObjId +
                  " and O.proj_id_ = " + entityIdProjId;

            sqlStringProject = 
                @"select P.name_, P.password_, P.id_
                  from projects P 
                  where P.id_ =   " + entityIdProjId;

            sqlStringData = 
                @"select NULL as id_mon_data, D.time_, D.lat_, D.lon_, 
                         D.speed_, D.add_info_ 
                  from basedata D 
                  where D.obj_id_ =  " + entityIdObjId +
                  " and D.proj_id_ = " + entityIdProjId +
                  " and D.time_ >= '" + dateTime.ToShortDateString() + "'";

            FbConnection conn = new FbConnection(this.ServerSettings.ServiceConnectionString);
            FbCommand data = new FbCommand(sqlStringObject, conn);
            conn.Open();
            FbDataReader reader = data.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    mkrkEntity.Id = "";
                    mkrkEntity.ObjName = reader.SafeGetString(1);
                    mkrkEntity.ObjId = entityIdObjId;
                    mkrkEntity.DtModify = reader.SafeGetDateTime(3);
                    mkrkEntity.Lon = reader.SafeGetString(4);
                    mkrkEntity.Lat = reader.SafeGetString(5);
                    mkrkEntity.Speed = reader.SafeGetString(6);
                    mkrkEntity.AddInfo = reader.SafeGetString(7);
                    mkrkEntity.ProjId = entityIdProjId;
                    mkrkEntity.Phone = "";
                    mkrkEntity.IdProject = "";
                    mkrkEntity.IdRko = "";
                }
            }

            reader.Close();
            data.CommandText = sqlStringProject;
            reader = data.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    mkrkEntity.ProjName = reader.SafeGetString(0);
                    mkrkEntity.Password = reader.SafeGetString(1);
                    mkrkEntity.IdRegion = reader.SafeGetString(2);
                }
            }

            reader.Close();
            conn.Close();

            FbConnection conn2 = new FbConnection(this.ServerSettings.MkrkBaseDataConnectionString);
            FbCommand data2 = new FbCommand(sqlStringData, conn2);
            conn2.Open();
            reader = data2.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    MkrkData newData = new MkrkData();
                    newData.Id = reader.SafeGetString(0);
                    newData.MonTime = reader.SafeGetDateTime(1);
                    newData.Lat = reader.SafeGetString(2);
                    newData.Lon = reader.SafeGetString(3);
                    newData.Speed = reader.SafeGetString(4);
                    newData.AddInfo = reader.SafeGetString(5);
                    mkrkEntity.AddData(newData);
                }
            }

            reader.Close();
            conn2.Close();
            return mkrkEntity;
        }
    }

}

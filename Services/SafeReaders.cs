﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.IO;
using System.Xml.Serialization;
using FirebirdSql.Data.FirebirdClient;
using Topshelf;

namespace RepServer
{
    public static class SafeReaders
    {
        public static string SafeGetString(this SqlDataReader reader, int colIndex)
        {
            if (!reader.IsDBNull(colIndex))
                return reader.GetString(colIndex);
            else
                return string.Empty;
        }

        public static DateTime SafeGetDateTime(this SqlDataReader reader, int colIndex)
        {
            var s = "2000-01-01 00:01";
            if (!reader.IsDBNull(colIndex))
                return reader.GetDateTime(colIndex);
            else
                return Convert.ToDateTime(s);
        }

        public static double SafeGetDouble(this SqlDataReader reader, int colIndex)
        {
            if (!reader.IsDBNull(colIndex))
                return reader.GetDouble(colIndex);
            else
                return Convert.ToDouble(0);
        }

        public static int SafeGetInt32(this SqlDataReader reader, int colIndex)
        {
            if (!reader.IsDBNull(colIndex))
                return reader.GetInt32(colIndex);
            else
                return Convert.ToInt32(0);
        }

        public static short SafeGetInt16(this SqlDataReader reader, int colIndex)
        {
            if (!reader.IsDBNull(colIndex))
                return reader.GetInt16(colIndex);
            else
                return Convert.ToInt16(0);
        }

        public static Decimal SafeGetDecimal(this SqlDataReader reader, int colIndex)
        {
            if (!reader.IsDBNull(colIndex))
                return reader.GetDecimal(colIndex);
            else
                return Convert.ToDecimal(0);
        }

        public static bool SafeGetBool(this SqlDataReader reader, int colIndex)
        {
            if (!reader.IsDBNull(colIndex))
                return reader.GetBoolean(colIndex);
            else
                return false;
        }

        //***methods for firebird
        public static string SafeGetString(this FbDataReader reader, int colIndex)
        {
            if (!reader.IsDBNull(colIndex))
                return reader.GetString(colIndex);
            else
                return string.Empty;
        }

        public static DateTime SafeGetDateTime(this FbDataReader reader, int colIndex)
        {
            var s = "1990-01-01 00:01";
            if (!reader.IsDBNull(colIndex))
                return reader.GetDateTime(colIndex);
            else
                return Convert.ToDateTime(s);
        }

        public static double SafeGetDouble(this FbDataReader reader, int colIndex)
        {
            if (!reader.IsDBNull(colIndex))
                return reader.GetDouble(colIndex);
            else
                return Convert.ToDouble(0);
        }

        public static int SafeGetInt32(this FbDataReader reader, int colIndex)
        {
            if (!reader.IsDBNull(colIndex))
                return reader.GetInt32(colIndex);
            else
                return Convert.ToInt32(0);
        }

        public static short SafeGetInt16(this FbDataReader reader, int colIndex)
        {
            if (!reader.IsDBNull(colIndex))
                return reader.GetInt16(colIndex);
            else
                return Convert.ToInt16(0);
        }

        public static Decimal SafeGetDecimal(this FbDataReader reader, int colIndex)
        {
            if (!reader.IsDBNull(colIndex))
                return reader.GetDecimal(colIndex);
            else
                return Convert.ToDecimal(0);
        }

        public static bool SafeGetBool(this FbDataReader reader, int colIndex)
        {
            if (!reader.IsDBNull(colIndex))
                return reader.GetBoolean(colIndex);
            else
                return false;
        }
    }
}

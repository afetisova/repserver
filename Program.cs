﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.IO;
using System.Xml.Serialization;
using FirebirdSql.Data.FirebirdClient;
using Topshelf;

namespace RepServer
{
    class Program
    {
        private static bool MkrkReplicationMode { get; set; }

        private static void Main(string[] args)
        {
            bool ConsoleMode = false;

            foreach (var s in args)
            {
                if (s.ToLower() == "console")
                {
                    ConsoleMode = true;
                }
            }
        
            if (!ConsoleMode)
            {
                HostFactory.Run(x =>
                {
                    x.Service<ServerService>(s => //2
                    {
                        s.ConstructUsing(name => new ServerService()); //3
                        s.WhenStarted(tc => tc.Start()); //4
                        s.WhenStopped(tc => tc.Stop()); //5
                    });
                    x.RunAsLocalSystem(); //6
                    x.SetDescription("Служба репликации данных АСРК"); //7
                    x.SetDisplayName("ASRK Data Replication Service"); //8
                    x.SetServiceName("ASRKReplicationService");
                });
            }
            else
            {
                Stream stream = new FileStream("server_settings.xml", FileMode.Open);
                XmlSerializer xmlSerializer = new XmlSerializer(typeof (Settings));
                Settings settings = (Settings) xmlSerializer.Deserialize(stream);

                stream.Close();

                Uri baseAddress = new Uri(settings.ServiceUrl + ":" + settings.ServicePort.ToString());
                // Create the ServiceHost.
                using (ServiceHost host = new ServiceHost(typeof (ReplicationService), baseAddress))
                {
                    // Enable metadata publishing.
                    ServiceMetadataBehavior smb = new ServiceMetadataBehavior
                    {
                        HttpGetEnabled = true,
                        MetadataExporter = {PolicyVersion = PolicyVersion.Policy15}
                    };
                    //ServiceBehaviorAttribute sba = new ServiceBehaviorAttribute();
                    //sba.
                    host.Description.Behaviors.Add(smb);

                    // Open the ServiceHost to start listening for messages. Since
                    // no endpoints are explicitly configured, the runtime will create
                    // one endpoint per base address for each service contract implemented
                    // by the service.
                    host.Open();
                    Console.WriteLine("The service is ready at {0}", baseAddress);
                    Console.WriteLine("Press <Enter> to stop the service.");
                    Console.WriteLine("Server provider: " + settings.ServiceProvider);
                    Console.WriteLine("Replication source: " + settings.ServiceConnectionString);
                    Console.ReadLine();

                    // Close the ServiceHost.
                    host.Close();
                }
            }
            //using (Stream writer = new FileStream("server_settings.xml", FileMode.Create))
            //{
            //    XmlSerializer xmlSerializer = new XmlSerializer(typeof(Settings));
            //    xmlSerializer.Serialize(writer, settings);

            //}

            //нормальный режим
            //Stream stream = new FileStream("server_settings.xml", FileMode.Open);
            //XmlSerializer xmlSerializer = new XmlSerializer(typeof(Settings));
            //Settings settings = (Settings) xmlSerializer.Deserialize(stream);

            //stream.Close();

            //Uri baseAddress = new Uri(settings.ServiceUrl + ":" + settings.ServicePort.ToString());
            //// Create the ServiceHost.
            //using (ServiceHost host = new ServiceHost(typeof (ReplicationService), baseAddress))
            //{
            //    // Enable metadata publishing.
            //    ServiceMetadataBehavior smb = new ServiceMetadataBehavior
            //    {
            //        HttpGetEnabled = true,
            //        MetadataExporter = {PolicyVersion = PolicyVersion.Policy15}
            //    };
            //    //ServiceBehaviorAttribute sba = new ServiceBehaviorAttribute();
            //    //sba.
            //    host.Description.Behaviors.Add(smb);

            //    // Open the ServiceHost to start listening for messages. Since
            //    // no endpoints are explicitly configured, the runtime will create
            //    // one endpoint per base address for each service contract implemented
            //    // by the service.
            //    host.Open();
            //    Console.WriteLine("The service is ready at {0}", baseAddress);
            //    Console.WriteLine("Press <Enter> to stop the service.");
            //    Console.WriteLine("Server provider: " + settings.ServiceProvider);
            //    Console.WriteLine("Replication source: " + settings.ServiceConnectionString);
            //    Console.ReadLine();

            //    // Close the ServiceHost.
            //    host.Close();
            //}
        }
    }
}

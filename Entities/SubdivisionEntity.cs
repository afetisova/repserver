﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepServer
{
    public class SubdivisionEntity
    {
        public string Id { get; set; }

        public string SubdivisionName { get; set; }

        public string SubdivisionShortName { get; set; }

        public string IdRegion { get; set; }

        public string IdParent { get; set; }

    }
}

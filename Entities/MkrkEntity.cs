﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepServer
{
    public class MkrkData
    {
        /// <summary>
        /// ID_MON_DATA
        /// </summary>
        public string Id { get; set; }

        public DateTime MonTime { get; set; }

        public string Lon { get; set; }

        public string Lat { get; set; }

        public string Speed { get; set; }
        
        public string AddInfo { get; set; }

    }

    public class MkrkEntity
    {
        private readonly IList<MkrkData> mkrkData = new List<MkrkData>();
        public IList<MkrkData> Data {get { return mkrkData; }}

        public void AddData(MkrkData value)
        {
            mkrkData.Add(value);
        }

        /// <summary>
        /// ID_MON_OBJECTS
        /// </summary>
        public string Id { get; set; }

        public string ObjName { get; set; }

        public string ObjId { get; set; }

        public string ProjId { get; set; }
    
        public string Lon { get; set; }

        public string Lat { get; set; }

        public string Speed { get; set; }
        
        public string AddInfo { get; set; }
        
        public string Phone { get; set; }
        
        /// <summary>
        /// ID_MON_PROJECTS
        /// </summary>
        public string IdProject { get; set; }
        
        public string IdRko { get; set; }
        
        public string ProjName { get; set; }
        
        public string Password { get; set; }
        

        public string IdRegion { get; set; }

        /// <summary>
        /// MON_OBJECT.LAST_TIME
        /// </summary>
        public DateTime DtModify { get; set; }

        public string RplIdServer { get; set; }
    }
    
}

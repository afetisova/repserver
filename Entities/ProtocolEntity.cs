﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepServer
{
    public class    CloudProtocolBand
    {
        /// <summary>
        /// ID_RK_PRTCL_BAND
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// FRQ_PERMITT
        /// </summary>
        public string PermitFreq { get; set; }

        /// <summary>
        /// BAND_MEASURED
        /// </summary>
        public string MeasuredBand { get; set; }

        /// <summary>
        /// BAND_POGR
        /// </summary>
        public string BandDelta { get; set; }

        /// <summary>
        /// BAND_ALLOW
        /// </summary>
        public string BandAllowed { get; set; }

        /// <summary>
        /// ID_RK_RAZMERNOST
        /// </summary>
        public string IdMeasureLevel { get; set; }

        /// <summary>
        /// IS_NORMALLY
        /// </summary>
        public bool IsValid { get; set; }

        /// <summary>
        /// ID_PRTCL_FRQ
        /// </summary>
        public string IdFreq { get; set; }

        /// <summary>
        /// RPL_ID_SERVER
        /// </summary>
        public string RplIdServer { get; set; }

    }

    public class CloudProtocolData
    {
        /// <summary>
        /// PROTOCOL_DATA_ID
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// ID_RK_ZADACHA
        /// </summary>
        public string IdTask { get; set; }

        /// <summary>
        /// IS_AUTO
        /// </summary>
        public bool IsAuto { get; set; }
        /// <summary>
        /// RPL_ID_SERVER
        /// </summary>
        public string RplIdServer { get; set; }

    }

    public class CloudProtocolEmiss
    {
        /// <summary>
        /// ID_RK_PRTCL_EMISS
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// LEVEL_EM
        /// </summary>
        public int EmissLevel { get; set; }

        /// <summary>
        /// BAND_EM
        /// </summary>
        public string EmissBand { get; set; }

        /// <summary>
        /// BAND_EM_POGR
        /// </summary>
        public string EmissBandDelta { get; set; }

        /// <summary>
        /// BAND_EM_ALLOW
        /// </summary>
        public string EmissBandAllow { get; set; }

        /// <summary>
        /// IS_NORMALLY
        /// </summary>
        public bool IsValid { get; set; }

        /// <summary>
        /// RPL_ID_SERVER
        /// </summary>
        public string RplIdServer { get; set; }

    }

    public class CloudProtocolEmp
    {
        /// <summary>
        /// ID_RK_PRTCL_EMP
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// MEASURE_DATE
        /// </summary>
        public DateTime MeasureDate { get; set; }

        /// <summary>
        /// MEASURE_LOCATION
        /// </summary>
        public string MeasureLocation { get; set; }

        /// <summary>
        /// MEASURE_GEO_LAT
        /// </summary>
        public string MeasureGeoLat { get; set; }

        /// <summary>
        /// MEASURE_GEO_LONG
        /// </summary>
        public string MeasureGeoLong { get; set; }

        /// <summary>
        /// MEASURE_AZIMUTH
        /// </summary>
        public string MeasureAzimuth { get; set; }

        /// <summary>
        /// MEASURE_DISTANCE
        /// </summary>
        public string MeasureDistance { get; set; }
        /// <summary>
        /// RPL_ID_SERVER
        /// </summary>
        public string RplIdServer { get; set; }

    }

    public class CloudProtocolFreq
    {
        /// <summary>
        /// ID_RK_PRTCL_FRQ
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// FRQ_PERMITT
        /// </summary>
        public string PermitFreq { get; set; }

        /// <summary>
        /// FRQ_MEASURED
        /// </summary>
        public string MeasuredFreq { get; set; }

        /// <summary>
        /// ID_RK_RAZMERNOST
        /// </summary>
        public string IdMeasureLevel { get; set; }

        /// <summary>
        /// FRQ_POGR
        /// </summary>
        public string FreqDelta { get; set; }

        /// <summary>
        /// FRQ_ALLOW_MIN
        /// </summary>
        public string FreqAllowMin { get; set; }

        /// <summary>
        /// FRQ_ALLOW_MAX
        /// </summary>
        public string FreqAllowMax { get; set; }

        /// <summary>
        /// IS_NORMALLY
        /// </summary>
        public bool IsValid { get; set; }

        /// <summary>
        /// IS_EXISTS
        /// </summary>
        public bool IsExists { get; set; }

        /// <summary>
        /// FRQ_MEASURED_FROM
        /// </summary>
        public string FreqMeasuredFrom { get; set; }

        /// <summary>
        /// FRQ_MEASURED_TO
        /// </summary>
        public string FreqMeasuredTo { get; set; }

        /// <summary>
        /// FRQ_PERMITT_FROM
        /// </summary>
        public string FreqPermitFrom { get; set; }

        /// <summary>
        /// FRQ_PERMITT_TO
        /// </summary>
        public string FreqPermitTo { get; set; }

        /// <summary>
        /// RPL_ID_SERVER
        /// </summary>
        public string RplIdServer { get; set; }

    }
    public class CloudProtocolRes
    {
        /// <summary>
        /// ID_RK_PRTCL_RES
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// CLIENT_NAME
        /// </summary>
        public string ClientName { get; set; }

        /// <summary>
        /// CLIENT_ADDRESS
        /// </summary>
        public string ClientAddress { get; set; }

        /// <summary>
        /// RES_FACTORY_ID
        /// </summary>
        public string FactoryId { get; set; }

        /// <summary>
        /// RES_LOCATION
        /// </summary>
        public string Location { get; set; }

        /// <summary>
        /// RES_NAME
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// RES_TYPE
        /// </summary>
        public string ResType { get; set; }

        /// <summary>
        /// RES_VID_ETS
        /// </summary>
        public string EtsVid { get; set; }

        /// <summary>
        /// RES_RADIOCLASS
        /// </summary>
        public string RadioClass { get; set; }

        /// <summary>
        /// RES_FREQ_RESOLUTION_NUM
        /// </summary>
        public string FreqResolutionNumber { get; set; }

        /// <summary>
        /// RES_FREQ_RESOLUTION_DATE_START
        /// </summary>
        public DateTime FreqResolutionStartDate { get; set; }

        /// <summary>
        /// RES_FREQ_RESOLUTION_DATE_END
        /// </summary>
        public DateTime FreqResolutionEndDate { get; set; }

        /// <summary>
        /// RES_CERTIFICATE_NUM
        /// </summary>
        public string CertificateNumber { get; set; }

        /// <summary>
        /// RES_CERTIFICATE_DATE_START
        /// </summary>
        public DateTime CertificateStartDate { get; set; }

        /// <summary>
        /// RES_CERTIFICATE_DATE_END
        /// </summary>
        public DateTime CertificateEndDate { get; set; }

        /// <summary>
        /// RES_GEO_LAT
        /// </summary>
        public string GeoLat { get; set; }

        /// <summary>
        /// RES_GEO_LONG
        /// </summary>
        public string GeoLong { get; set; }

        /// <summary>
        /// RES_ANTENNAHEIGHT
        /// </summary>
        public double AntennaHeight { get; set; }

        /// <summary>
        /// RES_POWER
        /// </summary>
        public double Power { get; set; }

        /// <summary>
        /// ID_SANCT_STATE
        /// </summary>
        public string IdSanctionState { get; set; }

        /// <summary>
        /// GEO_LAT_DELTA
        /// </summary>
        public string GeoLatDelta { get; set; }

        /// <summary>
        /// GEO_LONG_DELTA
        /// </summary>
        public string GeoLongDelta { get; set; }

        /// <summary>
        /// GEO_LAT_PERMIT
        /// </summary>
        public string GeoLatPermit { get; set; }

        /// <summary>
        /// GEO_LONG_PERMIT
        /// </summary>
        public string GeoLongPermit { get; set; }

        /// <summary>
        /// COORD_IS_NORMALLY
        /// </summary>
        public bool IsCoordinatesValid { get; set; }

        /// <summary>
        /// ANTHEIGHT_DELTA
        /// </summary>
        public double AntennaHeightDelta { get; set; }

        /// <summary>
        /// ANTHEIGHT_PERMIT
        /// </summary>
        public double AntennaHeightPermit { get; set; }

        /// <summary>
        /// RES_ANTHEIGHT_IS_NORMALLY
        /// </summary>
        public bool IsAntennaHeightValid { get; set; }

        /// <summary>
        /// NAME_TYPE_RADIOBUTTON
        /// </summary>
        public int VisualTypeSwitch { get; set; }

        /// <summary>
        /// ID_CERT_STATE
        /// </summary>
        public string IdCertificateState { get; set; }

        /// <summary>
        /// MANUAL_FREQ_DELTA
        /// </summary>
        public double ManualFreqDelta { get; set; }

        /// <summary>
        /// MANUAL_FREQ_DELTATYPE
        /// </summary>
        public int ManualFreqDeltaType { get; set; }

        /// <summary>
        /// MANUAL_BAND_DELTA
        /// </summary>
        public double ManualBandDelta { get; set; }

        /// <summary>
        /// MANUAL_BAND_DELTATYPE
        /// </summary>
        public int ManualBandDeltaType { get; set; }

        /// <summary>
        /// ID_RES_FREQ_RESOLUTION
        /// </summary>
        public string IdFreqResolution { get; set; }

        /// <summary>
        /// ID_RES_CERTIFICATE
        /// </summary>
        public string IdCertificate { get; set; }

        /// <summary>
        /// ID_AO_GU_CLIENT
        /// </summary>
        public string GuidClient { get; set; }

        /// <summary>
        /// ID_AO_GU_RES
        /// </summary>
        public string GuidRes { get; set; }

        /// <summary>
        /// RPL_ID_SERVER
        /// </summary>
        public string RplIdServer { get; set; }

    }

    public class CloudProtocolStrength
    {
        /// <summary>
        /// ID_RK_PRTCL_STRENGTH
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// FRQ_MEAS_MIN
        /// </summary>
        public string FreqMeasureMin { get; set; }

        /// <summary>
        /// FRQ_MEAS_MAX
        /// </summary>
        public string FreqMeasureMax { get; set; }

        /// <summary>
        /// ID_RK_RAZMERNOST
        /// </summary>
        public string IdMeasureLevel { get; set; }

        /// <summary>
        /// STRENGTH
        /// </summary>
        public string Strength { get; set; }

        /// <summary>
        /// STRENGTH_NORM
        /// </summary>
        public string StrengthNormal { get; set; }

        /// <summary>
        /// IS_NORMALLY
        /// </summary>
        public bool IsValid { get; set; }

        /// <summary>
        /// RPL_ID_SERVER
        /// </summary>
        public string RplIdServer { get; set; }

    }

    public class CLoudProtocolAuthors
    {
        public string Id { get; set; }
        public string IdAuthor { get; set; }
        /// <summary>
        /// RPL_ID_SERVER
        /// </summary>
        public string RplIdServer { get; set; }

    }

    public class ProtocolEntity
    {
        private readonly IList<CloudProtocolBand> band = new List<CloudProtocolBand>();
        private readonly IList<CloudProtocolEmiss> emiss = new List<CloudProtocolEmiss>();
        private readonly IList<CloudProtocolEmp> emp = new List<CloudProtocolEmp>();
        private readonly IList<CloudProtocolFreq> freq = new List<CloudProtocolFreq>();
        private readonly IList<CloudProtocolStrength> strength = new List<CloudProtocolStrength>();
        private readonly IList<CLoudProtocolAuthors> protocolAuthors = new List<CLoudProtocolAuthors>();


        /// <summary>
        /// RK_PRTCL_SOTRUDNIK [REL]
        /// </summary>
        public IList<CLoudProtocolAuthors> ProtocolAuthors { get { return protocolAuthors; } }

        public void AddProtocolAuthor(CLoudProtocolAuthors value)
        {
            protocolAuthors.Add(value);
        }

        public CLoudProtocolAuthors CreateProtocolAuthor()
        {
            return new CLoudProtocolAuthors();
        }



        /// <summary>
        /// PROTOCOL_DATA
        /// </summary>
        public CloudProtocolData ProtocolData { get; set; }

        public CloudProtocolData CreateProtocolData()
        {
            return new CloudProtocolData();
        }

        /// <summary>
        /// RK_PRTCL_RES
        /// </summary>
        public CloudProtocolRes Res { get; set; }

        public CloudProtocolRes CreateRes()
        {
            return new CloudProtocolRes();
        }

        /// <summary>
        /// RK_PRTCL_BAND
        /// </summary>
        public IList<CloudProtocolBand> Band { get { return band; } }

        public CloudProtocolBand CreateBand()
        {
            return new CloudProtocolBand();
        }

        public void AddBand(CloudProtocolBand value)
        {
            band.Add(value);
        }

        /// <summary>
        /// RK_PRTCL_EMISS
        /// </summary>
        public IList<CloudProtocolEmiss> Emiss { get { return emiss; } }

        public CloudProtocolEmiss CreatEmiss()
        {
            return new CloudProtocolEmiss();
        }

        public void AddEmiss(CloudProtocolEmiss value)
        {
            emiss.Add(value);
        }

        /// <summary>
        /// RK_PRTCL_EMP
        /// </summary>
        public IList<CloudProtocolEmp> Emp { get { return emp; } }

        public CloudProtocolEmp CreatEmp()
        {
            return new CloudProtocolEmp();
        }

        public void AddEmp(CloudProtocolEmp value)
        {
            emp.Add(value);
        }

        /// <summary>
        /// RK_PRTCL_FRQ
        /// </summary>
        public IList<CloudProtocolFreq> Freq { get { return freq; } }

        public CloudProtocolFreq CreateFreq()
        {
            return new CloudProtocolFreq();
        }

        public void AddFreq(CloudProtocolFreq value)
        {
            freq.Add(value);
        }

        /// <summary>
        /// RK_PRTCL_STRENGTH
        /// </summary>
        public IList<CloudProtocolStrength> Strength { get { return strength; } }

        public CloudProtocolStrength CreateStrength()
        {
            return new CloudProtocolStrength();
        }

        public void AddStrength(CloudProtocolStrength value)
        {
            strength.Add(value);
        }



        /// <summary>
        /// ID_PROTOCOL
        /// </summary>
        public Decimal Id { get; set; }

        /// <summary>
        /// NUM_PROTOCOL
        /// </summary>
        public string Number { get; set; }

        /// <summary>
        /// DATE_PROTOCOL
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// ID_CLIENT
        /// </summary>
        public string IdClient { get; set; }

        /// <summary>
        /// ID_RES
        /// </summary>
        public string IdRes { get; set; }

        /// <summary>
        /// ID_MEASUR_CONDITION (будет заменено на REL IList)
        /// </summary>
        public string IdMeasureCondition { get; set; }

        /// <summary>
        /// MEASUR_CONDITION (будет удалено)
        /// </summary>
        public string MeasureCondition { get; set; }

        /// <summary>
        /// ID_RK_EQUIPMENTSET (будет заменено на REL IList)
        /// </summary>
        public string IdEquipmentset { get; set; }

        /// <summary>
        /// EQUIPMENTSET (будет удалено)
        /// </summary>
        public string Equipmentset { get; set; }

        /// <summary>
        /// ID_METOD (будет заменено на REL IList) 
        /// </summary>
        public string IdMethodic { get; set; }

        /// <summary>
        /// METOD (будет удалено)
        /// </summary>
        public string Methodic { get; set; }

        /// <summary>
        /// DESCRIPTION
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// ZADANIE_NUM 
        /// </summary>
        public string TaskNumber { get; set; }

        /// <summary>
        /// ZADANIE_DATE
        /// </summary>
        public DateTime TaskDate { get; set; }

        /// <summary>
        /// ZAYAVKA_NOMER
        /// </summary>
        public string IssueNumber { get; set; }

        /// <summary>
        /// ZAYAVKA_DATE
        /// </summary>
        public DateTime IssueDate { get; set; }

        /// <summary>
        /// ID_SR_IZM
        /// </summary>
        public string IdRko { get; set; }

        /// <summary>
        /// ID_PROTOCOL_TYPE
        /// </summary>
        public string IdProtocolType { get; set; }

        /// <summary>
        /// NUMATTACH
        /// </summary>
        public string AttachmentCount { get; set; }

        /// <summary>
        /// ID_RK_TYPE_NARUSH
        /// </summary>
        public string IdViolationType { get; set; }

        /// <summary>
        /// DEPARTCOUNT
        /// </summary>
        public string DepartureCount { get; set; }

        /// <summary>
        /// MANHOUR
        /// </summary>
        public string ManHours { get; set; }

        /// <summary>
        /// PAGECOUNT
        /// </summary>
        public string PageCount { get; set; }

        /// <summary>
        /// ID_RK_REQUEST_TASK
        /// </summary>
        public string IdRequestTask { get; set; }

        /// <summary>
        /// ID_CLIENT_DBS
        /// </summary>
        public string IdClientDBS { get; set; }

        /// <summary>
        /// ID_RES_DBS
        /// </summary>
        public string IdResDBS { get; set; }

        /// <summary>
        /// ID_CLIENT_EIS
        /// </summary>
        public string IdClientEIS { get; set; }

        /// <summary>
        /// ID_RES_EIS
        /// </summary>
        public string IdResEIS { get; set; }

        /// <summary>
        /// ID_RK_REQUEST_RES
        /// </summary>
        public string IdRequestRes { get; set; }

        /// <summary>
        /// ID_RK_REQUEST_FRQ_ZONE
        /// </summary>
        public string IdRequestFreqZone { get; set; }

        /// <summary>
        /// ID_RK_REQUEST_FREQ
        /// </summary>
        public string IdRequestFreq { get; set; }

        /// <summary>
        /// ID_RK_REQUEST_REC
        /// </summary>
        public string IdRequestRec { get; set; }

        /// <summary>
        /// ID_RK_REQUEST_SANCT
        /// </summary>
        public string IdRequestSanct { get; set; }

        /// <summary>
        /// MANTISSA_FREQ
        /// </summary>
        public string MantissaFreq { get; set; }

        /// <summary>
        /// MANTISSA_BAND
        /// </summary>
        public string MantissaBand { get; set; }

        /// <summary>
        /// DT_MODIFY
        /// </summary>
        public DateTime DtModify { get; set; }

        /// <summary>
        /// IS_DELETED
        /// </summary>
        public int IsDeleted { get; set; }

        /// <summary>
        /// RPL_ID_SERVER
        /// </summary>
        public string RplIdServer { get; set; }

        

    }
}

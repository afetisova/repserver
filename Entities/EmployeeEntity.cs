﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepServer
{

    public class CloudEmployeeCertificate 
    {
        public string Id { get; set; }
  
        /// <summary>
        /// RK_PKI_USER_CERTIFICATES.NAME
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// RK_PKI_USER_CERTIFICATES.SERIAL_NUMBER
        /// </summary>
        public string SerialNumber { get; set; }

        /// <summary>
        /// RK_PKI_USER_CERTIFICATES.THUMBPRINT
        /// </summary>
        public string ThumbPrint { get; set; }

        /// <summary>
        /// RK_PKI_USER_CERTIFICATES.CERTIFICATE
        /// </summary>
        public string Certificate { get; set; }
    }


    public class EmployeeEntity
    {
        // ID == из родительского класса
        public string Id { get; set; }
    

        /// <summary>
        /// SOTRUDNIK.FAMIL
        /// </summary>		
        public string Surname { get; set; }

        /// <summary>
        /// SOTRUDNIK.NAME
        /// </summary>		
        public string Name { get; set; }

        /// <summary>
        /// SOTRUDNIK.OTCH
        /// </summary>
        public string SecondName { get; set; }

        /// <summary>
        /// SOTRUDNIK.PHONE
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// SOTRUDNIK.PHONE_WORK
        /// </summary>
        public string PhoneWork { get; set; }

        /// <summary>
        /// SOTRUDNIK.PHONE_WORK_PRN
        /// </summary>
        public string PhoneWorkPrn { get; set; }

        /// <summary>
        /// SOTRUDNIK.EMAIL
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// SOTRUDNIK.ID_OTDEL
        /// </summary>
        public string IdSubdivision { get; set; }

        /// <summary>
        /// SOTRUDNIK.L_LOCK
        /// </summary>
        public string Lock { get; set; }

        /// <summary>
        /// SOTRUDNIK.DATE_DISABLE
        /// </summary>
        public DateTime DisableDate { get; set; }

        /// <summary>
        /// SOTRUDNIK.ID_DL
        /// </summary>
        public string IdPosition { get; set; }

        /// <summary>
        /// SOTRUDNIK.DBS_ID
        /// </summary>
        public string IdDbs { get; set; }

        /// <summary>
        /// SOTRUDNIK.ID_POLZ_DB = POLZ_DB.ID_POLZ_DB
        /// </summary>
        //ссылка на запись из таблицы POLZ_DB. Поля в этой таблице дублируют аналогичные поля в таблице SOTRUDNIK и зачем  нужна POLZ_DB и нужна ли она вообще, мне затруднились сказать.
        //если что, тогда введем отдельный клауд для нее.
        public string IdDbUser { get; set; }

        /// <summary>
        /// SOTRUDNIK.SOTRUDNIK_LOGIN
        /// </summary>
        public string Login { get; set; }

        /// <summary>
        /// SOTRUDNIK.ID_USER_CERTIFICATE = RK_PKI_USER_CERTIFICATES
        /// </summary>
        public string IdUserCertificate { get; set; }

        public DateTime DtModify { get; set; }

        public string IsDeleted { get; set; }

        /// <summary>
        /// SOTRUDNIK.RPL_ID_SERVER
        /// </summary>		
        public string RplIdServer { get; set; }

        private readonly IList<CloudEmployeeCertificate> certificates = new List<CloudEmployeeCertificate>();

        public IList<CloudEmployeeCertificate> Certificates { get { return certificates; } }

        public CloudEmployeeCertificate CreateCertificate()
        {
            return new CloudEmployeeCertificate();
        }

        public void AddCertificate(CloudEmployeeCertificate value)
        {
            certificates.Add(value);
        }


    }
}

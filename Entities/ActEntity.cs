﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace RepServer
{

    public class CloudActWorkTime
    {
        /// <summary>
        /// ID_ACT_WORK_TIME
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// DTBEGIN
        /// </summary>
        public DateTime DateFrom { get; set; }

        /// <summary>
        /// DTEND
        /// </summary>
        public DateTime DateTo { get; set; }
    }

    public class CloudActStatus 
    {
        /// <summary>
        /// ID_REL_ACT_STATUS
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// ID_ACT_STATUS
        /// </summary>
        public string IdStatus { get; set; }

        /// <summary>
        /// ID_SOTRUDNIK
        /// </summary>
        public string IdSotrudnik { get; set; }

        /// <summary>
        /// STATUS_DATE
        /// </summary>
        public DateTime StatusDate { get; set; }

        /// <summary>
        /// MSG
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// RPL_ID_SERVER
        /// </summary>
        public string RplIdServer { get; set; }
    }

    public class CloudActErrorClass 
    {
        /// <summary>
        /// ID_ACT_ERROR_CLASS
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// CLASSIFICATION
        /// </summary>
        public string ErrorClass { get; set; }

        /// <summary>
        /// RPL_ID_SERVER
        /// </summary>
        public string RplIdServer { get; set; }
    }

    public class CloudActRequestDetails
    {
        /// <summary>
        /// ID_REL_ACT_REQUEST_DETAILS
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// REQUEST_TABLE_NAME
        /// </summary>
        public string TableName { get; set; }

        /// <summary>
        /// REQUEST_TABLE_ID_KEY
        /// </summary>
        public string TableKeyValue { get; set; }

        /// <summary>
        /// RPL_ID_SERVER
        /// </summary>
        public string RplIdServer { get; set; }
    }

    public class ActEntity
    {
        private readonly IList<CloudRel> protocols = new List<CloudRel>();
        private readonly IList<CloudRel> actPerformers = new List<CloudRel>();
        private readonly IList<CloudRel> rkoList = new List<CloudRel>();
        private readonly IList<CloudActWorkTime> workTimes = new List<CloudActWorkTime>();
        private readonly IList<CloudRel> tasks = new List<CloudRel>();
        private readonly IList<CloudActStatus> actStatuses = new List<CloudActStatus>();
        private readonly IList<CloudRel> actAuthors = new List<CloudRel>();
        private readonly IList<CloudActErrorClass> actErrorClasses = new List<CloudActErrorClass>();
        private readonly IList<CloudActRequestDetails> actRequestDetails = new List<CloudActRequestDetails>();

        /// <summary>
        /// ID_ACT_DATA
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// ACT_NUM
        /// </summary>
        public string Number { get; set; }

        /// <summary>
        /// ACT_DATE
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// ID_SP_ACTIVITY [SP_ACTIVITY]
        /// </summary>
        public string IdActivity { get; set; }

        /// <summary>
        /// OSNOVANIE
        /// </summary>
        public string ConditionAsText { get; set; }

        /// <summary>
        /// VYVOD
        /// </summary>
        public string Conclusion { get; set; }

        /// <summary>
        /// LOCATION
        /// </summary>
        public string Location { get; set; }

        /// <summary>
        /// PROTOCOL_DATA_STRING
        /// </summary>
        public string ProtocolsAsText { get; set; }

        /// <summary>
        /// ID_MAN_CONFIRM [SOTRUDNIK]
        /// </summary>
        public string IdConfirmer { get; set; }

        /// <summary>
        /// DEPARTCOUNT
        /// </summary>
        public int DepartureCount { get; set; }

        /// <summary>
        /// MANHOUR
        /// </summary>
        public double ManHours { get; set; }

        /// <summary>
        /// ID_RK_REQUEST_TASK [RK_REQUEST_TASK]
        /// </summary>
        public string IdRequestTasks { get; set; }

        public CloudRel CreateCloudRel()
        {
            return new CloudRel();
        }

        public IList<CloudRel> Protocols { get { return protocols; } }

        public void AddProtocol(CloudRel value)
        {
            protocols.Add(value);
        }

        /// <summary>
        /// ACT_PERFORMER [REL]
        /// </summary>
        public IList<CloudRel> ActPerformers { get { return actPerformers; } }

        public void AddPerformer(CloudRel value)
        {
            actPerformers.Add(value);
        }

        /// <summary>
        /// ACT_SR_IZM [REL]
        /// </summary>
        public IList<CloudRel> RkoList { get { return rkoList; } }

        public void AddRko(CloudRel value)
        {
            rkoList.Add(value);
        }

        /// <summary>
        /// ACT_WORK_TIME
        /// </summary>
        public IList<CloudActWorkTime> WorkTimes { get { return workTimes; } }

        public CloudActWorkTime CreateWorkTime()
        {
            return new CloudActWorkTime();
        }

        public void AddWorkTime(CloudActWorkTime value)
        {
            workTimes.Add(value);
        }

        /// <summary>
        /// REL_RK_ACT_ZADACHA [REL]
        /// </summary>
        public IList<CloudRel> Tasks { get { return tasks; } }

        public void AddTask(CloudRel value)
        {
            tasks.Add(value);
        }

        /// <summary>
        /// RK_REL_ACT_STATUSES [REL]
        /// </summary>
        public IList<CloudActStatus> ActStatuses { get { return actStatuses; } }

        public CloudActStatus CreateActStatus()
        {
            return new CloudActStatus();
        }

        public void AddActStatus(CloudActStatus value)
        {
            actStatuses.Add(value);
        }

        /// <summary>
        /// RK_ACT_ACTUAL_LINKS.ID_REL_ACT_STATUS
        /// </summary>
        public string IdCurrentStatus { get; set; }

        /// <summary>
        /// RK_REL_ACT_AUTHORS
        /// </summary>
        public IList<CloudRel> ActAuthors { get { return actAuthors; } }

        public void AddActAuthor(CloudRel value)
        {
            actAuthors.Add(value);
        }

        /// <summary>
        /// RK_REL_ACT_ERROR_CLASS
        /// </summary>
        public IList<CloudActErrorClass> ActErrorClasses { get { return actErrorClasses; } }

        public CloudActErrorClass CreateActErrorClass()
        {
            return new CloudActErrorClass();
        }

        public void AddActErrorClass(CloudActErrorClass value)
        {
            actErrorClasses.Add(value);
        }

        /// <summary>
        /// RK_REL_ACT_REQUEST_DETAILS [?]
        /// </summary>
        public IList<CloudActRequestDetails> ActRequestDetails { get { return actRequestDetails; } }

        public CloudActRequestDetails CreateActRequestDetail()
        {
            return new CloudActRequestDetails();
        }

        public void AddActRequestDetail(CloudActRequestDetails value)
        {
            actRequestDetails.Add(value);
        }

        /// <summary>
        /// DT_MODIFY
        /// </summary>
        public DateTime DtModify { get; set; }

        /// <summary>
        /// IS_DELETED
        /// </summary>
        public int IsDeleted { get; set; }

        /// <summary>
        /// RPL_ID_SERVER
        /// </summary>
        public string RplIdServer { get; set; }


    }

}

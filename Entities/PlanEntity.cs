﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepServer
{
        public class CloudPlanDetailFreq
        {

            /// <summary>
            /// ID_RK_AGGR_PLANDETAIL_FRQ
            /// </summary>
            public string Id{ get; set; }

            /// <summary>
            ///ID_RK_AGGR_PLANDETAIL
            /// </summary>
            public string IdPlanDetail { get; set; }

            /// <summary>
            ///ID_RK_PLAN_FRQ_LIST
            /// </summary>
            public string IdFrqList { get; set; }

            /// <summary>
            ///FREQ
            /// </summary>
            public string Freq { get; set; }

            /// <summary>
            ///FREQ_INT
            /// </summary>
            public string FreqInt { get; set; }

            /// <summary>
            ///FREQ_WIDTH
            /// </summary>
            public string FreqWidth { get; set; }

            /// <summary>
            ///FREQ_WIDTH_INT
            /// </summary>
            public string FreqWidthInt { get; set; }

            /// <summary>
            ///ID_RK_RAZMERNOST
            /// </summary>
            public string IdRazm { get; set; }

            /// <summary>
            ///LOFREQ
            /// </summary>
            public string FreqStart { get; set; }

            /// <summary>
            ///HIFREQ
            /// </summary>
            public string FreqEnd { get; set; }

            /// <summary>
            ///FRQ_STEP
            /// </summary>
            public string FreqStep { get; set; }

            /// <summary>
            ///ID_SP_BAND_DIAP
            /// </summary>
            public string IdBandDiap { get; set; }

            /// <summary>
            ///ID_SP_BAND_FRQ
            /// </summary>
            public string IdBandFrq { get; set; }

            /// <summary>
            ///LOFREQ_INT		
            /// </summary>
            public string FreqStartInt { get; set; }

            /// <summary>
            ///HIFREQ_INT	
            /// </summary>
            public string FreqEndInt { get; set; }

            /// <summary>
            ///FRQ_STEP_INT	
            /// </summary>
            public string FreqStepInt { get; set; }

            /// <summary>
            ///RPL_ID_SERVER		
            /// </summary>
            public string RplIdServer { get; set; }

        }


        public class PlanDetailEntity 
        {
            private readonly IList<CloudPlanDetailFreq> planDetailFreq = new List<CloudPlanDetailFreq>();

            public IList<CloudPlanDetailFreq> PlanDetailFreq { get { return planDetailFreq; } }

            public CloudPlanDetailFreq CreateFreq()
            {
                return new CloudPlanDetailFreq();
            }

            public void AddFreq(CloudPlanDetailFreq value)
            {
                planDetailFreq.Add(value);
            }

            /// <summary>
            ///ID_RK_AGGR_PLANDETAIL
            /// </summary>
            public string Id { get; set; }

            /// <summary>
            ///ID_RK_PLANDETAIL
            /// </summary>
            public string IdPlanDetailBd { get; set; }

            /// <summary>
            ///ID_REGION
            /// </summary>
            public string IdRegion { get; set; }

            /// <summary>
            ///ID_RK_PLAN
            /// </summary>
            public string IdPlan { get; set; }

            /// <summary>
            ///ID_RES
            /// </summary>
            public string IdRes { get; set; }

            /// <summary>
            ///STARTDAT
            /// </summary>
            public DateTime DtStart { get; set; }

            /// <summary>
            ///ENDDAT
            /// </summary>
            public DateTime DtEnd { get; set; }

            /// <summary>
            ///ID_ZADACHA
            /// </summary>
            public string IdZadacha { get; set; }

            /// <summary>
            ///ID_PROTOCOL
            /// </summary>
            public string IdProtocol { get; set; }

            /// <summary>
            ///ID_ACT_DATA
            /// </summary>
            public string IdAct { get; set; }

            /// <summary>
            ///ID_SP_TYPE_FREQ_TASK
            /// </summary>
            public string IdFreqTask { get; set; }

            /// <summary>
            ///FREQ
            /// </summary>
            public string Freq { get; set; }

            /// <summary>
            ///FRQSTART
            /// </summary>
            public string FreqStart { get; set; }

            /// <summary>
            ///FRQEND		
            /// </summary>
            public string FreqEnd { get; set; }

            /// <summary>
            ///STEP		
            /// </summary>
            public string FreqStep { get; set; }

            /// <summary>
            ///ID_RKO		
            /// </summary>
            public string IdRko { get; set; }

            /// <summary>
            ///ID_OBJECT		
            /// </summary>
            public string IdObject { get; set; }

            /// <summary>
            ///ID_RK_RAZMERNOST		
            /// </summary>
            public string IdRazm { get; set; }

            /// <summary>
            ///ID_OPERATION		
            /// </summary>
            public string IdOperation { get; set; }

            /// <summary>
            ///IS_MARK_DEL		
            /// </summary>
            public string IsMarkDel { get; set; }

            /// <summary>
            ///IS_MOBILE		
            /// </summary>
            public string IsMobile { get; set; }

            /// <summary>
            ///ID_PERIOD		
            /// </summary>
            public string IdPeriod { get; set; }

            /// <summary>
            ///ID_CLIENT		
            /// </summary>
            public string IdClient { get; set; }

            /// <summary>
            ///ID_RK_PL_ST_OBJ_PER		
            /// </summary>
            public string IdStObjPeriod { get; set; }

            /// <summary>
            ///ID_RK_PL_MOB_OBJ_PER		
            /// </summary>
            public string IdMobObjPeriod { get; set; }

            /// <summary>
            ///ID_RK_PLAN_FRQ_DETAIL		
            /// </summary>
            public string IdFrqDetail { get; set; }

            /// <summary>
            ///ID_RK_PLAN_REQUEST_OBJ		
            /// </summary>
            public string IdRequestObj { get; set; }

            /// <summary>
            ///ID_SOTRUDNIK		
            /// </summary>
            public string IdSotrudnik { get; set; }

            /// <summary>
            ///ID_TRASSA		
            /// </summary>
            public string IdTrassa { get; set; }

            /// <summary>
            ///ID_POINT		
            /// </summary>
            public string IdPoint { get; set; }

            /// <summary>
            ///ID_SANCTION_L		
            /// </summary>
            public string IdSanctionL { get; set; }

            /// <summary>
            ///ID_SANCTION		
            /// </summary>
            public string IdSanction { get; set; }

            /// <summary>
            ///ID_TYPE_IN_PLAN		
            /// </summary>
            public string IdType { get; set; }

            /// <summary>
            ///ID_RK_MTD		
            /// </summary>
            public string IdMtd { get; set; }

            /// <summary>
            ///TIME_CONTROL		
            /// </summary>
            public string TimeControl { get; set; }

            /// <summary>
            ///IS_KV		
            /// </summary>
            public string IsKv { get; set; }

            /// <summary>
            ///ID_OPERATION_KV		
            /// </summary>
            public string IdOperationKv { get; set; }

            /// <summary>
            ///ID_RASPISANIE		
            /// </summary>
            public string IdRaspisanie { get; set; }

            /// <summary>
            ///ID_RK_REQUEST_TASK		
            /// </summary>
            public string IdTask { get; set; }

            /// <summary>
            ///ID_CLIENT_DBS		
            /// </summary>
            public string IdClientDbs { get; set; }

            /// <summary>
            ///ID_RES_DBS		
            /// </summary>
            public string IdResDbs { get; set; }

            /// <summary>
            ///ID_CLIENT_EIS		
            /// </summary>
            public string IdClientEis { get; set; }

            /// <summary>
            ///ID_RES_EIS		
            /// </summary>
            public string IdResEis { get; set; }

            /// <summary>
            ///START_RECORD		
            /// </summary>
            public string StartRec { get; set; }

            /// <summary>
            ///END_RECORD		
            /// </summary>
            public string EndRec { get; set; }

            /// <summary>
            ///TITLE		
            /// </summary>
            public string Title { get; set; }

            /// <summary>
            ///ID_CHANNEL		
            /// </summary>
            public string IdChannel { get; set; }

            /// <summary>
            ///ID_RK_PLAN_RECORD		
            /// </summary>
            public string IdPlanRec { get; set; }

            /// <summary>
            ///ID_RK_PLAN_MONITORING		
            /// </summary>
            public string IdPlanMon { get; set; }

            /// <summary>
            ///ID_RK_REQUEST_TASK_RES		
            /// </summary>
            public string IdTaskRes { get; set; }

            /// <summary>
            ///ID_SP_TYPE_PLAN_TASK		
            /// </summary>
            public string IdTypePlanTask { get; set; }

            /// <summary>
            ///COUNT_RES		
            /// </summary>
            public string CountRes { get; set; }

            /// <summary>
            ///COUNT_FRQ		
            /// </summary>
            public string CountFrq { get; set; }

            /// <summary>
            ///RPL_ID_SERVER		
            /// </summary>
            public string RplIdServer { get; set; }

            /// <summary>
            ///F_STRING		
            /// </summary>
            public string FString { get; set; }

            /// <summary>
            ///REQUEST_NUM		
            /// </summary>
            public string NumRequest { get; set; }

            /// <summary>
            ///REQUEST_DATE		
            /// </summary>
            public DateTime DateRequest { get; set; }

            public DateTime  DtModify { get; set; }

            
        }

}

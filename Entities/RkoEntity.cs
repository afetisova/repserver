﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepServer
{
    public class CloudActRko
    {
        public string Id { get; set; }
        public string Act { get; set; }
        public string RplIdServer { get; set; }
    }

    public class CloudRkoMetrology 
    {
        public string Id { get; set; }
    
        /// <summary>
        /// CERT_NUM
        /// </summary>
        public string CertNum { get; set; }

        /// <summary>
        /// DATE_FROM
        /// </summary>
        public DateTime CertDateFrom { get; set; }

        /// <summary>
        /// DATE_TO
        /// </summary>
        public DateTime CertDateTo { get; set; }

        /// <summary>
        /// FILENAME
        /// </summary>
        public string CertFileName { get; set; }

        /// <summary>
        /// PAGES
        /// </summary>
        public string CertPageCount { get; set; }

        /// <summary>
        /// CERT_ORG
        /// </summary>
        public string CertAuthority { get; set; }
    }
    
    
    public class RkoEntity 
    {
        // ID == RK_RKO_COMPLEX.COMPLEX_NAME

        public string Id { get; set; }
    
        /// <summary>
        /// SP_SREDSTV.ID_SP_SREDSTV == RK_SP_RKO_TYPES.ID_RKO_TYPES
        /// </summary>
        public string IdRkoType { get; set; }

        /// <summary>
        /// SP_SREDSTV.NAME_SR == RK_SP_RKO_TYPES.TYPE_NAME
        /// </summary>
        public string RkoTypeName { get; set; }

        /// <summary>
        /// SP_TIP_SREDSTVA.ID_SP_TIP_SR (RK_SR_IZM, SP_SREDSTV) == RK_SP_RKO_CATEGORY.ID_RKO_CATEGORY
        /// </summary>
        public string IdSpTipSredstva { get; set; }

        /// <summary>
        /// SP_TIP_SREDSTVA.NAME == RK_SP_RKO_CATEGORY.CATEGORY_NAME
        /// </summary>
        public string TipSredstvaName { get; set; }

        /// <summary>
        /// RK_SR_PODRAZD.ID_SR_PODRAZD
        /// </summary>
        public string IdSrPodrazd { get; set; }

        /// <summary>
        /// RK_SR_PODRAZD.ID_SR_IZM == RK_POST.ID_POST
        /// </summary>
        public string IdPost { get; set; }

        /// <summary>
        /// SP_SREDSTV.IZGOTOVIT == RK_SR_IZM.IZGOTOVOIT
        /// </summary>
        public string VendorName { get; set; }

    //    public string IdVendor { get; set; }
    
        
        /// <summary>
        /// SP_SREDSTV.ID_NAZN_SREDSTVA
        /// </summary>
        public string IdNaznSredstva { get; set; }

        /// <summary>
        /// SP_SREDSTV.ID_GROUP
        /// </summary>
        public string IdGroup { get; set; }

        /// <summary>
        /// RK_SR_IZM.is_rk_30mhzless (flag for link to RK_REL_COMPLEX_SUBSYSTEM)
        /// </summary>
        public string IsLessThan30Mhz { get; set; }

        /// <summary>
        /// RK_SR_IZM.is_rk_30mhzgreat (flag for link to RK_REL_COMPLEX_SUBSYSTEM)
        /// </summary>
        public string IsMoreThan30Mhz { get; set; }

        /// <summary>
        /// RK_SR_IZM.is_rk_satelite (flag for link to RK_REL_COMPLEX_SUBSYSTEM)
        /// </summary>
        public string IsSatellite { get; set; }

        /// <summary>
        /// RK_SR_IZM.IS_MOBILE
        /// </summary>
        public string IsMobile { get; set; }

        /// <summary>
        /// RK_SR_IZM.IS_PORTABLE
        /// </summary>
        public string IsPortable { get; set; }

        /// <summary>
        /// (SP_SREDSTV.FULL_NAME --> RK_SR_IZM.DISCRIPTION --> SP_SREDSTV.NAME_SR) == RK_RKO.RKO_NAME == RK_RKO_COMPLEX.COMPLEX_NAME
        /// </summary>
        public string RkoName { get; set; }

        /// <summary>
        /// RK_SR_IZM.DAT_VYP
        /// </summary>
        public string RkoMadeDate { get; set; }

        /// <summary>
        /// RK_SR_IZM.DAT_VVOD == RK_RKO.RKO_DATE
        /// </summary>
        public DateTime RkoDate { get; set; }

        /// <summary>
        /// RK_SR_IZM.S_GPS
        /// </summary>
        public string IsHaveGPS { get; set; }

        /// <summary>
        /// RK_SR_IZM.BNOTFAIS
        /// </summary>
        public string IsNotForFAIS { get; set; }

        /// <summary>
        /// RK_SR_IZM.IS_POST
        /// </summary>
        public string IsPost { get; set; }

        /// <summary>
        /// RK_SR_IZM.IS_INDICATOR
        /// </summary>
        public string IsIndicator { get; set; }

        /// <summary>
        /// RK_SR_IZM.ZAVOD_NUM == RK_RKO.FACTORY_NUMBER
        /// </summary>
        public string FactoryNumber { get; set; }

        /// <summary>
        /// RK_SR_IZM.IPADDR == RK_RKO.IP_ADDRESS
        /// </summary>
        public string IpAddress { get; set; }

        /// <summary>
        /// RK_SR_IZM.INV_NUM == RK_RKO.RKO_INV_NUMBER
        /// </summary>
        public string InvNumber { get; set; }

        /// <summary>
        /// RK_SR_IZM.ID_PLACE
        /// </summary>
        public string IdPlace { get; set; }

        /// <summary>
        /// RK_PLACE.SHIROTA
        /// </summary>
        public string GeoLat { get; set; }

        /// <summary>
        /// RK_PLACE.DOLGOTA
        /// </summary>
        public string GeoLong { get; set; }

        /// <summary>
        /// RK_PLACE.ID_RK_ADDRESS
        /// </summary>
        public string IdAddress { get; set; }

        /// <summary>
        /// RK_ADDRESS.ID_RK_REGION
        /// </summary>
        public string IdRegion { get; set; }

        /// <summary>
        /// RK_ADDRESS.POSTINDEX
        /// </summary>
        public string PostIndex { get; set; }

        /// <summary>
        /// RK_ADDRESS.FULL_ADDRESS
        /// </summary>
        public string PostAddress { get; set; }

        /// <summary>
        /// RK_ADDRESS.ID_ADR_KLADR
        /// </summary>
        public string KladrAddress { get; set; }

        /// <summary>
        /// RK_ADDRESS.ID_AO_GU
        /// </summary>
        public string FiasAddress { get; set; }

        /// <summary>
        /// RK_SR_IZM.ID_TS_CATEGORY
        /// </summary>
        public string IdTsCategory { get; set; }

        /// <summary>
        /// RK_SR_IZM.ID_TS_STATE
        /// </summary>
        public string IdTsState { get; set; }

        public DateTime DtModify { get; set; }

        private readonly IList<CloudActRko> acts = new List<CloudActRko>();

        public IList<CloudActRko> Acts { get { return acts; } }

        public CloudActRko CreateActs()
        {            
         return       new CloudActRko();
        }
        
        public void AddAct(CloudActRko value)
        {
            acts.Add(value);
        }

        private readonly IList<CloudRkoMetrology> metrology = new List<CloudRkoMetrology>();
        /// <summary>
        /// Свидетельства о поверке РКО
        /// </summary>
        public IList<CloudRkoMetrology> Metrology { get { return metrology; } }

        public CloudRkoMetrology CreateRkoMetrology()
        {
            return new CloudRkoMetrology();
        }

        public void AddRkoMetrology(CloudRkoMetrology value)
        {
            metrology.Add(value);
        }

        public string PostName { get; set; }

        public string SubvisionName { get; set; }

        public string IdSubdivision { get; set; }

        public string RplIdServer { get; set; }
        public string SubdivisionRplIdServer { get; set; }
        public string PostRplIdServer { get; set; }
        public string ComplexRplIdServer { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepServer
{
    public class Settings
    {
        public string ServiceUrl { get; set; }
        public uint ServicePort { get; set; }
        public string ServiceProvider { get; set; }
        public string ServiceConnectionString { get; set; }
        public string MkrkBaseDataConnectionString { get; set; }
        // public string MkrkBaseProjectsConnectionString { get; set; }
    }

    //public class MkrkReplicationSettings
    //{
    //    public string ServiceUrl { get; set; }
    //    public uint ServicePort { get; set; }        
    //    public string BaseDataConnectionString { get; set; }
    //    public string BaseProjectsConnectionString { get; set; }
    //    public string RkoId { get; set; }    
    //}
}

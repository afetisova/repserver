﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepServer
{
    public class CloudRel
    {
        public string Id { get; set; }
        public string ForeignKeyId { get; set; }
        public string RplIdServer { get; set; }
    
    }
}

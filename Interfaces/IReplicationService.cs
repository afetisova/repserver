﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.IO;
using System.Xml.Serialization;
using FirebirdSql.Data.FirebirdClient;
using Topshelf;

namespace RepServer
{
    [ServiceContract]
    public interface IReplicationService
    {
        [OperationContract]
        DateTime GetMaxDate(string entity, string regionId);

        [OperationContract]
        IList<string> GetEntityIdList(string entity, DateTime maxdate);

        [OperationContract]
        ProtocolEntity GetProtocolEntity(string entityId);

        [OperationContract]
        ActEntity GetActEntity(string entityId);

        [OperationContract]
        RkoEntity GetOldSchemaRkoEntity(string entityId);

        [OperationContract]
        SubdivisionEntity GetSubdivisionEntity(string entityId);

        [OperationContract]
        EmployeeEntity GetEmployeeEntity(string entityId);

        [OperationContract]
        PlanDetailEntity GetPlanDetailEntity(string entityId);

        [OperationContract]
        MkrkEntity GetMkrkEntity(string entityId);

        [OperationContract]
        MkrkEntity GetMkrkEntityFromServerMkrk(string entityIdProjId, string entityIdObjId, DateTime dtModify);
    }
}
